package com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener;

public interface OnShareImageListener {
	public void onShareImage(String imagePath);
	public void onShareFrame(String imagePath);
}
