package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads.HuziConstants;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads.HuziLoadShowInter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.config.ALog;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.config.Constant;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener.OnChooseColorListener;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener.OnShareImageListener;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.multitouch.controller.ImageEntity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.multitouch.controller.MultiTouchEntity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.multitouch.custom.PhotoView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.SavePreviewActivity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ImageUtils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ResultContainer;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

import collagemaker.photoeditor.colorpicker.ColorPickerDialog;
import collagemaker.photoeditor.utils.DateTimeUtils;
import collagemaker.photoeditor.utils.PhotoUtils;
import io.paperdb.Paper;

public class PhotoCollageFragment extends BaseFragment
//        OnDoubleClickListener, OnEditImageMenuClickListener,

//        OnAddImageButtonClickListener, OnBorderShadowOptionListener, ColorPickerDialog.OnColorChangedListener {
{
    //action id


    public static boolean checksave = false;

    private OnShareImageListener mShareImageListener;
    private ImageView mDeletePhotoView;
    private ImageView mBorderView;
    private ImageView mInfoView;
    private PhotoView mPhotoView;
    private ViewGroup mPhotoLayout;

    private int mItemType = Constant.NORMAL_IMAGE_ITEM;
    // edit image
    private ImageEntity mSelectedEntity = null;
    // use for animation, these views are found from dialogs
    private Animation mAnimation;
    private int mPhotoViewWidth;
    private int mPhotoViewHeight;
    private SharedPreferences mPreferences;
    private OnChooseColorListener mChooseColorListener;
    private int mCurrentColor = Color.WHITE;
    private ColorPickerDialog mColorPickerDialog;

    ImageView show_image, save_btn;

    Bitmap image_result;


    HuziLoadShowInter envy_inter;
    String google_ad_ids, fb_ad_ids;
    private void loadInterFirst() {

        google_ad_ids = getString(R.string.InterstitialAd_id);
        fb_ad_ids = getString(R.string.facebook_interstitial_one);

        envy_inter.loadInterstitial(getActivity(), google_ad_ids, fb_ad_ids);
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        mPreferences = activity.getSharedPreferences(Constant.PREF_NAME,
                Context.MODE_PRIVATE);
        try {
            mShareImageListener = (OnShareImageListener) activity;
            if (activity instanceof OnChooseColorListener) {
                mChooseColorListener = (OnChooseColorListener) activity;
            }
        } catch (ClassCastException e) {
            throw new ClassCastException(e.getMessage());
        }

        pickMultipleImageFromGallery();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (savedInstanceState != null) {
            ResultContainer.getInstance().restoreFromBundle(savedInstanceState);
            mPhotoViewWidth = savedInstanceState
                    .getInt(Constant.PHOTO_VIEW_WIDTH_KEY);
            mPhotoViewHeight = savedInstanceState
                    .getInt(Constant.PHOTO_VIEW_HEIGHT_KEY);
        }
        mAnimation = AnimationUtils.loadAnimation(mActivity,
                R.anim.slide_in_bottom);
//        createQuickAction();


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        ResultContainer.getInstance().saveToBundle(outState);
        outState.putInt(Constant.PHOTO_VIEW_WIDTH_KEY, mPhotoViewWidth);
        outState.putInt(Constant.PHOTO_VIEW_HEIGHT_KEY, mPhotoViewHeight);
        outState.putParcelableArrayList("imageEntities", mPhotoView.getImageEntities());
        outState.putParcelable("backgroundUri", mPhotoView.getPhotoBackgroundUri());
        outState.putParcelable("mSelectedEntity", mSelectedEntity);
    }




    private void refreshAd(final View rootView) {
        AdLoader adLoader = new AdLoader.Builder(getActivity(), getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {
                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tv_loading.setVisibility(View.GONE);
                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = rootView.findViewById(R.id.fl_adplaceholder3);
                        NativeAdView adView3 = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ad_splash, null);
                        Utils.populateFullAdavanceNative_Splash(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                        // Show the ad.
                    }
                })
//                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
//                    @Override
//                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//
//
//                    }
//                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd(rootView);
                        super.onAdFailedToLoad(loadAdError);
                    }

                    @Override
                    public void onAdImpression() {
//                        check_native = true;
//
//                        if (check_native && check_inter) {
//                            next_screen();
//
//                            if (mInterstitialAd != null) {
//                                mInterstitialAd.show(SplashActivity.this);
//                            } else if (fb_interstitial != null) {
//                                if (fb_interstitial.isAdLoaded()) {
//                                    fb_interstitial.show();
//                                }
//                            }
//                        }
                        super.onAdImpression();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd(View rootView) {

        final NativeAdLayout nativeAdLayout = rootView.findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(getActivity(), getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }

                progressBar.setVisibility(View.GONE);
                tv_loading.setVisibility(View.GONE);
                // Inflate Native Ad into Container
                Utils.fb_full_nativeadvance_for_splash(getActivity(), fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//                check_native = true;
//
//                if (check_native && check_inter) {
//                    next_screen();
//
//                    if (mInterstitialAd != null) {
//                        mInterstitialAd.show(SplashActivity.this);
//                    } else if (fb_interstitial != null) {
//                        if (fb_interstitial.isAdLoaded()) {
//                            fb_interstitial.show();
//                        }
//                    }
//                }
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    RelativeLayout ad_main;
    ProgressBar progressBar;
    TextView tv_loading;

    @SuppressLint("NewApi")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ALog.d("PhotoCollageFragment.onCreateView", "onCreateView");
        View rootView = inflater.inflate(R.layout.fragment_photocollage,
                container, false);
        mPhotoLayout = (ViewGroup) rootView.findViewById(R.id.photoLayout);

        Paper.init(getActivity());


        ad_main = rootView.findViewById(R.id.ad_stars);
        nativead = rootView.findViewById(R.id.nativead);
        fb_main = rootView.findViewById(R.id.fb_main);
        progressBar = rootView.findViewById(R.id.progressbar);
        tv_loading = rootView.findViewById(R.id.tv_loading);
        Boolean status = Utils.isInternetAvailable(getActivity());

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            nativead.setVisibility(View.VISIBLE);
            refreshAd(rootView);
        } else {
            ad_main.setVisibility(View.GONE);
            fb_main.setVisibility(View.GONE);
            nativead.setVisibility(View.GONE);
        }



        envy_inter = HuziLoadShowInter.getInstance(getActivity());
        loadInterFirst();



        mPhotoView = new PhotoView(getActivity());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
        mPhotoLayout.addView(mPhotoView, params);
        mPhotoView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {

                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {
                        mPhotoViewWidth = mPhotoView.getWidth();
                        mPhotoViewHeight = mPhotoView.getHeight();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                            mPhotoView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            mPhotoView.getViewTreeObserver()
                                    .removeGlobalOnLayoutListener(this);
                        }

                    }
                });

//        mPhotoView.setOnDoubleClickListener(this);

        if (savedInstanceState != null) {
            ArrayList<MultiTouchEntity> entities = savedInstanceState.getParcelableArrayList("imageEntities");
            if (entities != null) {
                mPhotoView.setImageEntities(entities);
            }
            Uri backgroundUri = savedInstanceState.getParcelable("backgroundUri");
            if (backgroundUri != null) {
                mPhotoView.setPhotoBackground(backgroundUri);
            }
            ImageEntity entity = savedInstanceState.getParcelable("mSelectedEntity");
            if (entity != null) {
                mSelectedEntity = entity;
            }
        } else {
            mPhotoView.setImageEntities(ResultContainer.getInstance()
                    .copyImageEntities());

            if (ResultContainer.getInstance().getPhotoBackgroundImage() != null) {
                mPhotoView.setPhotoBackground(ResultContainer.getInstance()
                        .getPhotoBackgroundImage());
            }
        }
//
        mActivity = getActivity();
        mActivity.setTitle(
                R.string.create_from_photo);


        show_image = rootView.findViewById(R.id.show_image);
        save_btn = rootView.findViewById(R.id.save_btn);


        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checksave = true;
                clickShareView();

                HuziConstants.show_inter_ad(getActivity(), null);


            }
        });
        HuziConstants.show_inter_ad(getActivity(), null);

        return rootView;
    }

    @Override
    public void onPause() {
        super.onPause();
        ALog.d("PhotoCollageFragment.onPause",
                "onPause: width=" + mPhotoView.getWidth() + ", height = "
                        + mPhotoView.getHeight());
        mPhotoView.unloadImages();
    }

    @Override
    public void onResume() {
        super.onResume();
        ALog.d("PhotoCollageFragment.onResume",
                "onResume: width=" + mPhotoView.getWidth() + ", height = "
                        + mPhotoView.getHeight());
        mPhotoView.loadImages(getActivity());
        mPhotoView.invalidate();
    }

    @Override
    public void resultFromPhotoEditor(Uri uri) {
        super.resultFromPhotoEditor(uri);

        ALog.d("PhotoCollageFragment.resultFromPhotoEditor", "uri=" + uri.toString());
        if (!already()) {
            return;
        }

        if (mItemType != Constant.BACKGROUND_ITEM) {
            ImageEntity entity = new ImageEntity(uri, getResources());
            entity.setSticker(false);
            entity.load(getActivity(),
                    (mPhotoViewWidth - entity.getWidth()) / 2,
                    (mPhotoViewHeight - entity.getHeight()) / 2);
            mPhotoView.addImageEntity(entity);
            if (ResultContainer.getInstance().getImageEntities() != null) {
                ResultContainer.getInstance().getImageEntities().add(entity);
            }

        } else {
            mPhotoView.setPhotoBackground(uri);
            ResultContainer.getInstance().setPhotoBackgroundImage(uri);
        }
    }

    @Override
    protected void resultEditImage(Uri uri) {
        if (uri == null) {
            getActivity().finish();
        } else {
            super.resultEditImage(uri);
            mSelectedEntity.setImageUri(getActivity(), uri);

//        final Bitmap image = mPhotoView.getImage(ImageUtils.calculateOutputScaleFactor(400, 300));
            image_result = loadBitmap(uri);

            show_image.setImageBitmap(image_result);
            mPhotoView.invalidate();
        }


    }

    public Bitmap loadBitmap(Uri url) {
        Bitmap bitmap = null;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), url);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return bitmap;
    }


    @Override
    public void resultPickMultipleImages(Uri[] uri) {
        super.resultPickMultipleImages(uri);
        if (!already()) {
            return;
        }
        final int size = uri.length;
        ImageEntity entity = null;
        for (int idx = 0; idx < size; idx++) {
            float angle = (float) (idx * Math.PI / 20);

            entity = new ImageEntity(uri[idx], getResources());
            entity.setInitScaleFactor(0.5f);
            entity.setSticker(false);
            entity.load(getActivity(),
                    (mPhotoViewWidth - entity.getWidth()) / 2,
                    (mPhotoViewHeight - entity.getHeight()) / 2, angle);
            mPhotoView.addImageEntity(entity);
            if (ResultContainer.getInstance().getImageEntities() != null) {
                ResultContainer.getInstance().getImageEntities().add(entity);
            }
        }
//        onEditButtonClick();
        mSelectedEntity = entity;
        requestEditingImage(mSelectedEntity.getImageUri());
    }

    public void clickShareView() {
        if (!already()) {
            return;
        }
        mActivity = getActivity();
//        final Bitmap image = mPhotoView.getImage(ImageUtils.calculateOutputScaleFactor(mPhotoView.getWidth(), mPhotoView.getHeight()));
        final Bitmap image = image_result;
        AsyncTask<Void, Void, File> task = new AsyncTask<Void, Void, File>() {
            Dialog dialog;
            String errMsg;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(mActivity, getString(R.string.app_name), getString(R.string.creating));
            }

            @Override
            protected File doInBackground(Void... params) {
                try {
                    String fileName = DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png");
                    File collageFolder = new File(ImageUtils.OUTPUT_COLLAGE_FOLDER);
                    if (!collageFolder.exists()) {
                        collageFolder.mkdirs();
                    }
                    File photoFile = new File(collageFolder, fileName);
                    image.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(photoFile));
                    PhotoUtils.addImageToGallery(photoFile.getAbsolutePath(), mActivity);
                    return photoFile;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    errMsg = ex.getMessage();
                } catch (OutOfMemoryError err) {
                    err.printStackTrace();
                    errMsg = err.getMessage();
                }
                return null;
            }

            @Override
            protected void onPostExecute(File file) {
                super.onPostExecute(file);
                dialog.dismiss();
                Toast.makeText(mActivity, "Image saved successfully", Toast.LENGTH_SHORT).show();
                save_btn.setImageResource(R.drawable.save_green);
                save_btn.setClickable(false);
//
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ALog.d("PhotoCollageFragment.onDestroyView", "Destroy view");
        mPhotoView.unloadImages();
        mPhotoView.setImageEntities(null);
        mPhotoView.destroyBackground();
    }


}
