package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ImageUtils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import collagemaker.photoeditor.adapter.EditedImageAdaper;
import collagemaker.photoeditor.model.EditedImageItem;
import collagemaker.photoeditor.ui.activity.ImageProcessingActivity;

import collagemaker.photoeditor.utils.DialogUtils;

public class SavedActivity extends AppCompatActivity {
    private Animation mAnimation;
    private View mEditImageView;
    private Dialog mEditImageDialog;
    private GridView mGridView;
    private EditedImageAdaper mImageAdapter;
    private List<EditedImageItem> mImages = new ArrayList<EditedImageItem>();
    private DialogUtils.EditedImageLongClickListener mImageClickListener;




    RelativeLayout ad_main;
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    @SuppressLint("MissingPermission")
    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {

                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);

                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        com.google.android.gms.ads.nativead.NativeAdView adView3 = (com.google.android.gms.ads.nativead.NativeAdView) getLayoutInflater().inflate(R.layout.nativebanner, null);
                        Utils.BannerUnifiedNativeAdView(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();

//                        Toast.makeText(SplashActivity.this, errorCode+"", Toast.LENGTH_SHORT).show();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Toast.makeText(MainActivity.this, "Native ad failed to load: " + adError.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                Utils.fb_inflateAd(SavedActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
//                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saved);


        ad_main = findViewById(R.id.ad_main);
        nativead = findViewById(R.id.nativead);
        fb_main = findViewById(R.id.fb_main);
        Boolean status = Utils.isInternetAvailable(this);

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            refreshAd();
        } else {
            ad_main.setVisibility(View.GONE);
        }

        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        mGridView = (GridView) findViewById(R.id.gridView);
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent data = new Intent(SavedActivity.this, ViewImageActivity.class);
                data.putExtra(ViewImageActivity.IMAGE_FILE_KEY, mImages.get(position).getImage());
                startActivity(data);
            }
        });

        mImageClickListener = new DialogUtils.EditedImageLongClickListener() {

            @Override
            public void onShareButtonClick() {
                mEditImageDialog.dismiss();
                EditedImageItem item = getImageItem();
                if (item != null) {
                    String imagePath = item.getImage();





//                    File imagePath = new File(getApplicationContext().getCacheDir(), "images");
                    File newFile = new File(imagePath);
                    Uri contentUri = FileProvider.getUriForFile(SavedActivity.this, "com.photoeditor.layout.collagemaker.photocollage.filter.frames.provider", newFile);

                    if (contentUri != null) {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                        shareIntent.setDataAndType(contentUri, getContentResolver().getType(contentUri));
                        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                        startActivity(Intent.createChooser(shareIntent, "Choose an app"));
                    }





                    // postPhoto(imagePath);
//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("image/jpeg");
//                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(imagePath)));
//                    startActivity(Intent.createChooser(share, getString(dauroi.photoeditor.R.string.photo_editor_share_image)));
                }
            }

            @Override
            public void onEditButtonClick() {
                mEditImageDialog.dismiss();
                EditedImageItem item = getImageItem();
                if (item != null) {
                    Intent i = new Intent(SavedActivity.this, ImageProcessingActivity.class);
                    Uri mImageUri = Uri.fromFile(new File(item.getImage()));
                    i.putExtra(ImageProcessingActivity.IMAGE_URI_KEY, mImageUri);
                    i.putExtra(ImageProcessingActivity.IS_EDITING_IMAGE_KEY, true);
                    startActivityForResult(i, 994);
                }
            }

            @Override
            public void onDeleteButtonClick() {
                mEditImageDialog.dismiss();
                DialogUtils.showCoolConfirmDialog(SavedActivity.this, R.string.app_name, R.string.photo_editor_confirm_delete_image, new DialogUtils.ConfirmDialogOnClickListener() {
                    @Override
                    public void onOKButtonOnClick() {
                        EditedImageItem item = getImageItem();
                        if (item != null) {
                            File file = new File(item.getThumbnail());
                            file.delete();
                            mImages.remove(item);
                            mImageAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onCancelButtonOnClick() {

                    }
                });
            }
        };

        mEditImageDialog = DialogUtils.createEditImageDialog(SavedActivity.this, mImageClickListener, false);
        mEditImageView = mEditImageDialog.findViewById(R.id.dialogEditImage);
        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                if (mEditImageDialog != null && !mEditImageDialog.isShowing()) {
//                    mImageClickListener.setImageItem(mImages.get(position));
//                    mEditImageView.startAnimation(mAnimation);
//                    mEditImageDialog.show();
//                }
                return true;
            }
        });

        mImageAdapter = new EditedImageAdaper(SavedActivity.this, mImages);
        mGridView.setAdapter(mImageAdapter);
        mAnimation = AnimationUtils.loadAnimation(SavedActivity.this, R.anim.photo_editor_slide_in_bottom);


    }
    @Override
    public void onResume() {
        super.onResume();
        loadUserImagesAsync();
    }

    private void loadUserImagesAsync() {
        AsyncTask<Void, EditedImageItem, Void> task = new AsyncTask<Void, EditedImageItem, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                File thumbnailFolder = new File(ImageUtils.OUTPUT_COLLAGE_FOLDER);
                File[] editedThumbnails = thumbnailFolder.listFiles();
                EditedImageItem[] items = null;
                if (editedThumbnails != null && editedThumbnails.length > 0) {
                    final int len = editedThumbnails.length;
                    items = new EditedImageItem[len];
                    for (int idx = 0; idx < len; idx++) {
                        File file = editedThumbnails[idx];
                        EditedImageItem item = new EditedImageItem();
                        item.setThumbnail(file.getAbsolutePath());
                        File thumbnail = new File(ImageUtils.OUTPUT_COLLAGE_FOLDER.concat("/").concat(file.getName()));
                        item.setImage(thumbnail.getAbsolutePath());
                        items[idx] = item;
                    }
                }

                publishProgress(items);
                return null;
            }

            @Override
            protected void onProgressUpdate(EditedImageItem... values) {
                super.onProgressUpdate(values);
                if (values != null && values.length > 0) {
                    mImages.clear();
                    for (EditedImageItem item : values) {
                        mImages.add(item);
                    }

                    mImageAdapter.notifyDataSetChanged();
                }
            }

            @Override
            protected void onPostExecute(Void result) {
                super.onPostExecute(result);
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }
}