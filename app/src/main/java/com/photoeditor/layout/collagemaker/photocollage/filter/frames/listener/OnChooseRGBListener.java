package com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener;

public interface OnChooseRGBListener {
	public void onChooseRGB(int rgb);
}
