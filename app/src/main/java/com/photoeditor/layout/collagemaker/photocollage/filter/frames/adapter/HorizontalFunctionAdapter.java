package com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.Function_item;

import java.util.ArrayList;

public class HorizontalFunctionAdapter extends RecyclerView.Adapter<HorizontalFunctionAdapter.PreviewTemplateViewHolder> {
    public static class PreviewTemplateViewHolder extends RecyclerView.ViewHolder {
        private ImageView mImageView;
        private TextView mSelectedView;

        PreviewTemplateViewHolder(View itemView) {
            super(itemView);
            mImageView = (ImageView) itemView.findViewById(R.id.imageView);
            mSelectedView = itemView.findViewById(R.id.selectedView);
        }
    }

    public static interface OnPreviewTemplateClickListener {
        void onPreviewTemplateClick(Function_item item, int position);
    }

    private ArrayList<Function_item> mTemplateItems;
    private HorizontalFunctionAdapter.OnPreviewTemplateClickListener mListener;
Activity activity;
    public HorizontalFunctionAdapter(Activity activity,ArrayList<Function_item> items, HorizontalFunctionAdapter.OnPreviewTemplateClickListener listener) {
        mTemplateItems = items;
        mListener = listener;
        this.activity = activity;
    }

    @Override
    public HorizontalFunctionAdapter.PreviewTemplateViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_function, parent, false);
        return new HorizontalFunctionAdapter.PreviewTemplateViewHolder(v);
    }

    @Override
    public void onBindViewHolder(HorizontalFunctionAdapter.PreviewTemplateViewHolder holder, final int position) {
//        PhotoUtils.loadImageWithGlide(holder.mImageView.getContext(), holder.mImageView, mTemplateItems.get(position).getPreview());

        if (mTemplateItems.get(position).isSelected()) {
            holder.mSelectedView.setTextColor(activity.getResources().getColor(R.color.red_text));
            holder.mImageView.setImageResource(mTemplateItems.get(position).getIcon_selected());
        } else {
            holder.mSelectedView.setTextColor(activity.getResources().getColor(R.color.textColor));
            holder.mImageView.setImageResource(mTemplateItems.get(position).getIcon_unselected());
        }

        holder.mSelectedView.setText(mTemplateItems.get(position).getTitle());
        holder.mImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onPreviewTemplateClick(mTemplateItems.get(position),position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mTemplateItems.size();
    }
}
