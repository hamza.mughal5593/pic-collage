package com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

public class HuziConstants {
    public static void show_inter_ad(Activity mContext, Intent myintent) {

        HuziLoadShowInter interstitialAd = HuziLoadShowInter.getInstance(mContext);
        interstitialAd.showGoogleAndFbAd(myintent,mContext);

    }
}
