package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter.HorizontalFunctionAdapter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.frame.FrameImageView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.frame.FramePhotoLayout;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.Function_item;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.TemplateItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ImageUtils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import java.util.ArrayList;

import collagemaker.photoeditor.PhotoEditorApp;
import collagemaker.photoeditor.colorpicker.ColorPickerDialog;
import collagemaker.photoeditor.ui.activity.ImageProcessingActivity;
import collagemaker.photoeditor.utils.FileUtils;
import collagemaker.photoeditor.utils.ImageDecoder;


//public class FrameDetailActivity extends BaseTemplateDetailActivity implements FramePhotoLayout.OnQuickActionClickListener, ColorPickerDialog.OnColorChangedListener {
public class FrameDetailActivity extends BaseTemplateDetailActivity implements ColorPickerDialog.OnColorChangedListener {
    private static final int REQUEST_SELECT_PHOTO = 99;
    private static final float MAX_SPACE = ImageUtils.pxFromDp(PhotoEditorApp.getAppContext(), 30);
    private static final float MAX_CORNER = ImageUtils.pxFromDp(PhotoEditorApp.getAppContext(), 60);
    private static final float DEFAULT_SPACE = ImageUtils.pxFromDp(PhotoEditorApp.getAppContext(), 2);
    private static final float MAX_SPACE_PROGRESS = 300.0f;
    private static final float MAX_CORNER_PROGRESS = 200.0f;

    private FrameImageView mSelectedFrameImageView;
    private FramePhotoLayout mFramePhotoLayout;
    private SeekBar mSpaceBar;
    private SeekBar mCornerBar;
    private float mSpace = DEFAULT_SPACE;
    private float mCorner = 0;
    //Background
    private int mBackgroundColor = Color.BLACK;
    private Bitmap mBackgroundImage;
    private Uri mBackgroundUri = null;
    private ColorPickerDialog mColorPickerDialog;
    //Saved instance state
    private Bundle mSavedInstanceState;

    RecyclerView horitental_recyclar;
    protected HorizontalFunctionAdapter horizontalFunctionAdapter;
    ArrayList<Function_item> function_items = new ArrayList<>();


    LinearLayout ratio_main;

    ImageView fit_icon, square_icon, auto_icon;
    TextView fit_text, square_text, auto_text;
LinearLayout border_main;


    RelativeLayout ad_main;
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    @SuppressLint("MissingPermission")
    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {

                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);

                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        com.google.android.gms.ads.nativead.NativeAdView adView3 = (com.google.android.gms.ads.nativead.NativeAdView) getLayoutInflater().inflate(R.layout.nativebanner, null);
                        Utils.BannerUnifiedNativeAdView(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();

//                        Toast.makeText(SplashActivity.this, errorCode+"", Toast.LENGTH_SHORT).show();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Toast.makeText(MainActivity.this, "Native ad failed to load: " + adError.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                Utils.fb_inflateAd(FrameDetailActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
//                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }

    @Override
    protected boolean isShowingAllTemplates() {
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //restore old params
        if (savedInstanceState != null) {
            mSpace = savedInstanceState.getFloat("mSpace");
            mCorner = savedInstanceState.getFloat("mCorner");
            mBackgroundColor = savedInstanceState.getInt("mBackgroundColor");
            mBackgroundUri = savedInstanceState.getParcelable("mBackgroundUri");
            mSavedInstanceState = savedInstanceState;
            if (mBackgroundUri != null)
                mBackgroundImage = ImageDecoder.decodeUriToBitmap(this, mBackgroundUri);
        }


        ad_main = findViewById(R.id.ad_main);
        nativead = findViewById(R.id.nativead);
        fb_main = findViewById(R.id.fb_main);
        Boolean status = Utils.isInternetAvailable(this);

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            refreshAd();
        } else {
            ad_main.setVisibility(View.GONE);
        }



        //add ads view
//        addAdsView(R.id.adsLayout);
        //inflate widgets
        horitental_recyclar = findViewById(R.id.horitental_recyclar);
        ratio_main = findViewById(R.id.ratio_main);
        fit_icon = findViewById(R.id.fit_icon);
        square_icon = findViewById(R.id.square_icon);
        auto_icon = findViewById(R.id.auto_icon);
        fit_text = findViewById(R.id.fit_text);
        square_text = findViewById(R.id.square_text);
        auto_text = findViewById(R.id.auto_text);
        border_main = findViewById(R.id.border_main);

        mTemplateView.setVisibility(View.GONE);

        mAddImageDialog.findViewById(R.id.dividerTextView).setVisibility(View.VISIBLE);
        mAddImageDialog.findViewById(R.id.alterBackgroundView).setVisibility(View.VISIBLE);
        mAddImageDialog.findViewById(R.id.dividerBackgroundPhotoView).setVisibility(View.VISIBLE);
        mAddImageDialog.findViewById(R.id.alterBackgroundColorView).setVisibility(View.VISIBLE);
        mSpaceBar = (SeekBar) findViewById(R.id.spaceBar);
        mSpaceBar.setVisibility(View.GONE);


        function_items.add(new Function_item(R.drawable.borders_icon, R.drawable.border_red_icon,
                "Border", false));
        function_items.add(new Function_item(R.drawable.layout_icon, R.drawable.lauout_red_icon,
                "Layout", false));

        function_items.add(new Function_item(R.drawable.text_icon, R.drawable.text_red_icon,
                "Text", false));
        function_items.add(new Function_item(R.drawable.stickers_icon, R.drawable.sticker_red_icon,
                "Stickers", false));
//        function_items.add(new Function_item(R.drawable.frame_icon, R.drawable.frame_red_icon,
//                "Frames", false));
        function_items.add(new Function_item(R.drawable.ratio_icon, R.drawable.ratio_png_icon,
                "Ratio", false));
        function_items.add(new Function_item(R.drawable.background_icon, R.drawable.background_red_icon,
                "Background", false));
//        function_items.add(new Function_item(R.drawable.borders_icon,R.drawable.border_red_icon,
//                "Border",false));


        horizontalFunctionAdapter = new HorizontalFunctionAdapter(this, function_items, new HorizontalFunctionAdapter.OnPreviewTemplateClickListener() {
            @Override
            public void onPreviewTemplateClick(Function_item item, int position) {

                for (int i = 0; i < function_items.size(); i++) {
                    function_items.get(i).setSelected(false);
                }
                function_items.get(position).setSelected(true);
                horizontalFunctionAdapter.notifyDataSetChanged();

                mSpaceBar.setVisibility(View.GONE);
                mCornerBar.setVisibility(View.GONE);
                mTemplateView.setVisibility(View.GONE);
                ratio_main.setVisibility(View.GONE);
                border_main.setVisibility(View.GONE);

                if (item.getTitle().equalsIgnoreCase("Border")) {
                    mSpaceBar.setVisibility(View.VISIBLE);
                    mCornerBar.setVisibility(View.VISIBLE);
                    border_main.setVisibility(View.VISIBLE);
                } else if (item.getTitle().equalsIgnoreCase("Text")) {

                    FragmentManager fm = getSupportFragmentManager();
                    DialogFragment picker = new AddTextItemActivity(null, 0, null, new AddTextItemActivity.CustomDialogInterface() {
                        @Override
                        public void okButtonClicked(String text, int mTextColor, String mFontPath) {
                            resultAddTextItem(text, mTextColor, mFontPath);
                        }
                    });
                    picker.show(fm, "Date Picker");

                } else if (item.getTitle().equalsIgnoreCase("Layout")) {

                    mTemplateView.setVisibility(View.VISIBLE);


                } else if (item.getTitle().equalsIgnoreCase("Stickers")) {

                    pickSticker();


                } else if (item.getTitle().equalsIgnoreCase("Ratio")) {

                    ratio_main.setVisibility(View.VISIBLE);


                } else if (item.getTitle().equalsIgnoreCase("Background")) {

//                    pickBackground();
                    if (mColorPickerDialog == null) {
                        mColorPickerDialog = new ColorPickerDialog(FrameDetailActivity.this, mBackgroundColor);
                        mColorPickerDialog.setOnColorChangedListener(FrameDetailActivity.this);
                    }

                    mColorPickerDialog.setOldColor(mBackgroundColor);
                    if (!mColorPickerDialog.isShowing()) {
                        mColorPickerDialog.show();
                    }

                } else {
                    mSpaceBar.setVisibility(View.GONE);
                    mCornerBar.setVisibility(View.GONE);
                    mTemplateView.setVisibility(View.GONE);
                }

            }
        });
        //Show templates
        horitental_recyclar.setHasFixedSize(true);
        horitental_recyclar.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        horitental_recyclar.setAdapter(horizontalFunctionAdapter);

        if (mLayoutRatio == 0) {
            square_icon.setImageResource(R.drawable.square);
            square_text.setTextColor(getResources().getColor(R.color.red_text));
        } else if (mLayoutRatio == 1) {
            fit_icon.setImageResource(R.drawable.fit);
            fit_text.setTextColor(getResources().getColor(R.color.red_text));
        } else {

            auto_icon.setImageResource(R.drawable.auto);
            auto_text.setTextColor(getResources().getColor(R.color.red_text));
        }

        square_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                square_icon.setImageResource(R.drawable.square);
                square_text.setTextColor(getResources().getColor(R.color.red_text));


                fit_icon.setImageResource(R.drawable.fit_off);
                fit_text.setTextColor(getResources().getColor(R.color.textColor));

                auto_icon.setImageResource(R.drawable.auto_off);
                auto_text.setTextColor(getResources().getColor(R.color.textColor));

                mPref.edit().putInt(RATIO_KEY, 0).commit();
                mLayoutRatio = 0;
                buildLayout(mSelectedTemplateItem);


            }
        });

        fit_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fit_icon.setImageResource(R.drawable.fit);
                fit_text.setTextColor(getResources().getColor(R.color.red_text));


                square_icon.setImageResource(R.drawable.square_off);
                square_text.setTextColor(getResources().getColor(R.color.textColor));

                auto_icon.setImageResource(R.drawable.auto_off);
                auto_text.setTextColor(getResources().getColor(R.color.textColor));


                mPref.edit().putInt(RATIO_KEY, 1).commit();
                mLayoutRatio = 1;
                buildLayout(mSelectedTemplateItem);

            }
        });

        auto_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                auto_icon.setImageResource(R.drawable.auto);
                auto_text.setTextColor(getResources().getColor(R.color.red_text));


                square_icon.setImageResource(R.drawable.square_off);
                square_text.setTextColor(getResources().getColor(R.color.textColor));

                fit_icon.setImageResource(R.drawable.fit_off);
                fit_text.setTextColor(getResources().getColor(R.color.textColor));


                mPref.edit().putInt(RATIO_KEY, 2).commit();
                mLayoutRatio = 2;
                buildLayout(mSelectedTemplateItem);
            }
        });


        mSpaceBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mSpace = MAX_SPACE * seekBar.getProgress() / MAX_SPACE_PROGRESS;
                if (mFramePhotoLayout != null)
                    mFramePhotoLayout.setSpace(mSpace, mCorner);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mCornerBar = (SeekBar) findViewById(R.id.cornerBar);
        mCornerBar.setVisibility(View.GONE);
        mCornerBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                mCorner = MAX_CORNER * seekBar.getProgress() / MAX_CORNER_PROGRESS;
                if (mFramePhotoLayout != null)
                    mFramePhotoLayout.setSpace(mSpace, mCorner);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
        // show guide on first time
//        boolean show = mPreferences.getBoolean(Constant.SHOW_GUIDE_CREATE_FRAME_KEY, true);
//        if (show) {
//            clickInfoView();
//            mPreferences.edit().putBoolean(Constant.SHOW_GUIDE_CREATE_FRAME_KEY, false)
//                    .commit();
//        }
        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        findViewById(R.id.save_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asyncSaveAndShare();
            }
        });
    }
    @Override
    public void onBackPressed() {

        showEditDialog();
    }
    private void showEditDialog() {

        final Dialog dialog = new Dialog(FrameDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(collagemaker.photoeditor.R.layout.payment_alert_dialog);

        final TextView cancel = (TextView) dialog.findViewById(collagemaker.photoeditor.R.id.cancel);
        final TextView restart_btn = (TextView) dialog.findViewById(collagemaker.photoeditor.R.id.restart_btn);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();

            }
        });
        dialog.show();

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putFloat("mSpace", mSpace);
        outState.putFloat("mCornerBar", mCorner);
        outState.putInt("mBackgroundColor", mBackgroundColor);
        outState.putParcelable("mBackgroundUri", mBackgroundUri);
        if (mFramePhotoLayout != null) {
            mFramePhotoLayout.saveInstanceState(outState);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        boolean result = super.onCreateOptionsMenu(menu);
        menu.findItem(R.id.action_ratio).setVisible(true);
        return result;
    }

    @Override
    public void onBackgroundColorButtonClick() {
        if (mColorPickerDialog == null) {
            mColorPickerDialog = new ColorPickerDialog(this, mBackgroundColor);
            mColorPickerDialog.setOnColorChangedListener(this);
        }

        mColorPickerDialog.setOldColor(mBackgroundColor);
        if (!mColorPickerDialog.isShowing()) {
            mColorPickerDialog.show();
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_frame_detail;
    }

    @Override
    public Bitmap createOutputImage() throws OutOfMemoryError {
        try {
            Bitmap template = mFramePhotoLayout.createImage();
            Bitmap result = Bitmap.createBitmap(template.getWidth(), template.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);
            Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            if (mBackgroundImage != null && !mBackgroundImage.isRecycled()) {
                canvas.drawBitmap(mBackgroundImage, new Rect(0, 0, mBackgroundImage.getWidth(), mBackgroundImage.getHeight()),
                        new Rect(0, 0, result.getWidth(), result.getHeight()), paint);
            } else {
                canvas.drawColor(mBackgroundColor);
            }

            canvas.drawBitmap(template, 0, 0, paint);
            template.recycle();
            template = null;
            Bitmap stickers = mPhotoView.getImage(mOutputScale);
            canvas.drawBitmap(stickers, 0, 0, paint);
            stickers.recycle();
            stickers = null;
            System.gc();
            return result;
        } catch (OutOfMemoryError error) {
            throw error;
        }
    }

    @Override
    protected void buildLayout(TemplateItem item) {
        mFramePhotoLayout = new FramePhotoLayout(this, item.getPhotoItemList());
//        mFramePhotoLayout.setQuickActionClickListener(this);
        if (mBackgroundImage != null && !mBackgroundImage.isRecycled()) {
            if (Build.VERSION.SDK_INT >= 16)
                mContainerLayout.setBackground(new BitmapDrawable(getResources(), mBackgroundImage));
            else
                mContainerLayout.setBackgroundDrawable(new BitmapDrawable(getResources(), mBackgroundImage));
        } else {
            mContainerLayout.setBackgroundColor(mBackgroundColor);
        }

        int viewWidth = mContainerLayout.getWidth();
        int viewHeight = mContainerLayout.getHeight();
        if (mLayoutRatio == RATIO_SQUARE) {
            if (viewWidth > viewHeight) {
                viewWidth = viewHeight;
            } else {
                viewHeight = viewWidth;
            }
        } else if (mLayoutRatio == RATIO_GOLDEN) {
            final double goldenRatio = 1.61803398875;
            if (viewWidth <= viewHeight) {
                if (viewWidth * goldenRatio >= viewHeight) {
                    viewWidth = (int) (viewHeight / goldenRatio);
                } else {
                    viewHeight = (int) (viewWidth * goldenRatio);
                }
            } else if (viewHeight <= viewWidth) {
                if (viewHeight * goldenRatio >= viewWidth) {
                    viewHeight = (int) (viewWidth / goldenRatio);
                } else {
                    viewWidth = (int) (viewHeight * goldenRatio);
                }
            }
        }
        mOutputScale = ImageUtils.calculateOutputScaleFactor(viewWidth, viewHeight);
        mFramePhotoLayout.build(viewWidth, viewHeight, mOutputScale, mSpace, mCorner);
        if (mSavedInstanceState != null) {
            mFramePhotoLayout.restoreInstanceState(mSavedInstanceState);
            mSavedInstanceState = null;
        }
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(viewWidth, viewHeight);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mContainerLayout.removeAllViews();
        mContainerLayout.addView(mFramePhotoLayout, params);
        //add sticker view
        mContainerLayout.removeView(mPhotoView);
        mContainerLayout.addView(mPhotoView, params);
        //reset space and corner seek bars
        mSpaceBar.setProgress((int) (MAX_SPACE_PROGRESS * mSpace / MAX_SPACE));
        mCornerBar.setProgress((int) (MAX_CORNER_PROGRESS * mCorner / MAX_CORNER));
    }

//    @Override
//    public void onEditActionClick(FrameImageView v) {
//        mSelectedFrameImageView = v;
//        if (v.getImage() != null && v.getPhotoItem().imagePath != null && v.getPhotoItem().imagePath.length() > 0) {
//            Uri uri = Uri.fromFile(new File(v.getPhotoItem().imagePath));
//            requestEditingImage(uri);
//        }
//    }

//    @Override
//    public void onChangeActionClick(FrameImageView v) {
//        mSelectedFrameImageView = v;
//        requestPhoto();
//        Intent data = new Intent(this, SelectPhotoActivity.class);
//        data.putExtra(SelectPhotoActivity.EXTRA_IMAGE_COUNT, 1);
//        startActivityForResult(data, REQUEST_SELECT_PHOTO);
//    }

    @Override
    protected void resultEditImage(Uri uri) {
        if (mSelectedFrameImageView != null) {
            mSelectedFrameImageView.setImagePath(FileUtils.getPath(this, uri));
        }
    }

    @Override
    protected void resultFromPhotoEditor(Uri image) {
        if (mSelectedFrameImageView != null) {
            mSelectedFrameImageView.setImagePath(FileUtils.getPath(this, image));
        }
    }

    private void recycleBackgroundImage() {
        if (mBackgroundImage != null && !mBackgroundImage.isRecycled()) {
            mBackgroundImage.recycle();
            mBackgroundImage = null;
            System.gc();
        }
    }

    @Override
    protected void resultBackground(Uri uri) {
        recycleBackgroundImage();
        mBackgroundUri = uri;
        mBackgroundImage = ImageDecoder.decodeUriToBitmap(this, uri);
        if (Build.VERSION.SDK_INT >= 16)
            mContainerLayout.setBackground(new BitmapDrawable(getResources(), mBackgroundImage));
        else
            mContainerLayout.setBackgroundDrawable(new BitmapDrawable(getResources(), mBackgroundImage));
    }

    @Override
    public void onColorChanged(int color) {
        recycleBackgroundImage();
        mBackgroundColor = color;
        mContainerLayout.setBackgroundColor(mBackgroundColor);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_SELECT_PHOTO && resultCode == RESULT_OK) {
            ArrayList<String> mSelectedImages = data.getStringArrayListExtra(SelectPhotoActivity.EXTRA_SELECTED_IMAGES);
            if (mSelectedFrameImageView != null && mSelectedImages != null && !mSelectedImages.isEmpty()) {
                mSelectedFrameImageView.setImagePath(mSelectedImages.get(0));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void finish() {
        recycleBackgroundImage();
        if (mFramePhotoLayout != null) {
            mFramePhotoLayout.recycleImages();
        }
        super.finish();
    }


}
