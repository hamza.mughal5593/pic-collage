package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter.TemplateAdapter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter.TemplateViewHolder;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.TemplateItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.quickaction.QuickAction;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.quickaction.QuickActionItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.template.PhotoItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.TemplateImageUtils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.frame.FrameImageUtils;
//import com.google.firebase.crash.FirebaseCrash;
import com.tonicartos.superslim.LayoutManager;

import java.util.ArrayList;


public class TemplateActivity extends BaseFragmentActivity implements TemplateViewHolder.OnTemplateItemClickListener {
    private class ViewHolder {
        private final RecyclerView mRecyclerView;

        public ViewHolder(RecyclerView recyclerView) {
            mRecyclerView = recyclerView;
        }

        public void initViews(LayoutManager lm) {
            mRecyclerView.setLayoutManager(lm);
        }

        public void scrollToPosition(int position) {
            mRecyclerView.scrollToPosition(position);
        }

        public void setAdapter(RecyclerView.Adapter<?> adapter) {
            mRecyclerView.setAdapter(adapter);
        }

        public void smoothScrollToPosition(int position) {
            mRecyclerView.smoothScrollToPosition(position);
        }
    }

    public static final String EXTRA_IMAGE_PATHS = "imagePaths";
    public static final String EXTRA_IMAGE_IN_TEMPLATE_COUNT = "imageInTemplateCount";
    public static final String EXTRA_SELECTED_TEMPLATE_INDEX = "selectedTemplateIndex";
    public static final String EXTRA_IS_FRAME_IMAGE = "frameImage";

    private static final int REQUEST_SELECT_PHOTO = 789;

    private static final String KEY_HEADER_POSITIONING = "key_header_mode";

    private static final String KEY_MARGINS_FIXED = "key_margins_fixed";

    private ViewHolder mViews;

    private TemplateAdapter mAdapter;

    private int mHeaderDisplay;

    private boolean mAreMarginsFixed;

    //Template views
    private ArrayList<TemplateItem> mTemplateItemList = new ArrayList<TemplateItem>();
    private ArrayList<TemplateItem> mAllTemplateItemList = new ArrayList<TemplateItem>();
    private boolean mFrameImages = false;
    //Frame filter Quick Action
    private QuickAction mQuickAction;
    private TextView mFilterView;
    private int mImageInTemplateCount = 0;
    private int mSelectedTemplateIndex = 0;

//    @Override
//    protected void preCreateAdsHelper() {
//        mLoadedData = false;
//    }
ImageView back_btn;

    RelativeLayout ad_main;
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    @SuppressLint("MissingPermission")
    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {

                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);

                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        com.google.android.gms.ads.nativead.NativeAdView adView3 = (com.google.android.gms.ads.nativead.NativeAdView) getLayoutInflater().inflate(R.layout.nativebanner, null);
                        Utils.BannerUnifiedNativeAdView(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();

//                        Toast.makeText(SplashActivity.this, errorCode+"", Toast.LENGTH_SHORT).show();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Toast.makeText(MainActivity.this, "Native ad failed to load: " + adError.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                Utils.fb_inflateAd(TemplateActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
//                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_template);



        ad_main = findViewById(R.id.ad_main);
        nativead = findViewById(R.id.nativead);
        fb_main = findViewById(R.id.fb_main);
        Boolean status = Utils.isInternetAvailable(this);

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            refreshAd();
        } else {
            ad_main.setVisibility(View.GONE);
        }
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        if (savedInstanceState != null) {
            restoreInstanceState(savedInstanceState);
        } else {
            mHeaderDisplay = getResources().getInteger(R.integer.default_header_display);
            mAreMarginsFixed = getResources().getBoolean(R.bool.default_margins_fixed);
        }
        //inflate widgets
        mViews = new ViewHolder((RecyclerView) findViewById(R.id.recycler_view));
        mViews.initViews(new LayoutManager(this));
        //show templates
        mFrameImages = getIntent().getBooleanExtra(EXTRA_IS_FRAME_IMAGE, false);
        if (mFrameImages) {
            loadFrameImages(false);
        } else {
            loadFrameImages(true);
        }
        mAdapter = new TemplateAdapter(this, mHeaderDisplay, mTemplateItemList, this);
//        mAdapter.setMarginsFixed(mAreMarginsFixed);
        mAdapter.setMarginsFixed(false);
        mAdapter.setHeaderDisplay(mHeaderDisplay);
//        mAdapter.setHeaderDisplay(10);
        mViews.setAdapter(mAdapter);
        //Frame count filter
//        createFilterQuickAction();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_HEADER_POSITIONING, mHeaderDisplay);
        outState.putBoolean(KEY_MARGINS_FIXED, mAreMarginsFixed);
        outState.putBoolean(EXTRA_IS_FRAME_IMAGE, mFrameImages);
        outState.putInt("mImageInTemplateCount", mImageInTemplateCount);
        outState.putInt("mSelectedTemplateIndex", mSelectedTemplateIndex);
    }

    protected void restoreInstanceState(Bundle savedInstanceState) {
        mHeaderDisplay = savedInstanceState
                .getInt(KEY_HEADER_POSITIONING,
                        getResources().getInteger(R.integer.default_header_display));
        mAreMarginsFixed = savedInstanceState
                .getBoolean(KEY_MARGINS_FIXED,
                        getResources().getBoolean(R.bool.default_margins_fixed));
        mFrameImages = savedInstanceState.getBoolean(EXTRA_IS_FRAME_IMAGE, false);
        mImageInTemplateCount = savedInstanceState.getInt("mImageInTemplateCount");
        mSelectedTemplateIndex = savedInstanceState.getInt("mSelectedTemplateIndex");
    }

    private void loadFrameImages(boolean template) {
        mAllTemplateItemList.clear();
        if (template) {
            mAllTemplateItemList.addAll(TemplateImageUtils.loadTemplates());
        } else {
            mAllTemplateItemList.addAll(FrameImageUtils.loadFrameImages(this));
        }
        mTemplateItemList.clear();
        if (mImageInTemplateCount > 0) {
            for (TemplateItem item : mAllTemplateItemList)
                if (item.getPhotoItemList().size() == mImageInTemplateCount) {
                    mTemplateItemList.add(item);
                }
        } else {
            mTemplateItemList.addAll(mAllTemplateItemList);
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.filter_frame_count, menu);
//        MenuItem item = menu.findItem(R.id.action_filter);
//        mFilterView = (TextView) item.getActionView().findViewById(R.id.frameCountView);
//        mFilterView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mQuickAction.show(mFilterView);
//            }
//        });
//        return true;
//    }

//    private void createFilterQuickAction() {
//        //create QuickAction. Use QuickAction.VERTICAL or QuickAction.HORIZONTAL param to define layout
//        //orientation
//        mQuickAction = new QuickAction(this, QuickAction.VERTICAL);
//        mQuickAction.setPopupBackgroundColor(getResources().getColor(R.color.primaryColor));
//        //add action items into QuickAction
//        String[] filterTexts = getResources().getStringArray(R.array.frame_count);
//        if (mFrameImages) {
//            for (int idx = 0; idx < filterTexts.length; idx++) {
//                QuickActionItem item = new QuickActionItem(idx, filterTexts[idx]);
//                mQuickAction.addActionItem(item);
//            }
//        } else {
//            for (int idx = 0; idx < 4; idx++) {
//                QuickActionItem item = new QuickActionItem(idx, filterTexts[idx]);
//                mQuickAction.addActionItem(item);
//            }
//        }
//        //Set listener for action item clicked
//        mQuickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
//            @Override
//            public void onItemClick(QuickAction source, int pos, int actionId) {
//                QuickActionItem quickActionItem = mQuickAction.getActionItem(pos);
//                mQuickAction.dismiss();
//                //here we can filter which action item was clicked with pos or actionId parameter
//                mFilterView.setText(quickActionItem.getTitle());
//                if (quickActionItem.getActionId() == 0) {
//                    mTemplateItemList.clear();
//                    mTemplateItemList.addAll(mAllTemplateItemList);
//                    mImageInTemplateCount = 0;
//                } else {
//                    mTemplateItemList.clear();
//                    mImageInTemplateCount = quickActionItem.getActionId();
//                    for (TemplateItem item : mAllTemplateItemList)
//                        if (item.getPhotoItemList().size() == quickActionItem.getActionId()) {
//                            mTemplateItemList.add(item);
//                        }
//                }
//                mAdapter.setData(mTemplateItemList);
//            }
//        });
//
//        //set listnener for on dismiss event, this listener will be called only if QuickAction dialog was dismissed
//        //by clicking the area outside the dialog.
//        mQuickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
//            @Override
//            public void onDismiss() {
//
//            }
//        });
//    }

    @Override
    public void onTemplateItemClick(TemplateItem templateItem) {
        if (!templateItem.isAds()) {
            mSelectedTemplateIndex = mTemplateItemList.indexOf(templateItem);
            Intent data = new Intent(this, SelectPhotoActivity.class);
            data.putExtra(SelectPhotoActivity.EXTRA_IMAGE_COUNT, templateItem.getPhotoItemList().size());
            startActivityForResult(data, REQUEST_SELECT_PHOTO);
        } else {
            //TODO:
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_SELECT_PHOTO && resultCode == RESULT_OK) {
            try {
                ArrayList<String> mSelectedImages = data.getStringArrayListExtra(SelectPhotoActivity.EXTRA_SELECTED_IMAGES);
                final TemplateItem selectedTemplateItem = mTemplateItemList.get(mSelectedTemplateIndex);
                int itemSize = selectedTemplateItem.getPhotoItemList().size();
                int size = Math.min(itemSize, mSelectedImages.size());
                for (int idx = 0; idx < size; idx++) {
                    selectedTemplateItem.getPhotoItemList().get(idx).imagePath = mSelectedImages.get(idx);
                }
                Intent intent;
                if (mFrameImages) {
                    intent = new Intent(this, FrameDetailActivity.class);
                } else {
                    intent = new Intent(this, TemplateDetailActivity.class);
                }

                intent.putExtra(EXTRA_IMAGE_IN_TEMPLATE_COUNT, selectedTemplateItem.getPhotoItemList().size());
                intent.putExtra(EXTRA_IS_FRAME_IMAGE, mFrameImages);
                if (mImageInTemplateCount == 0) {
                    ArrayList<TemplateItem> tmp = new ArrayList<>();
                    for (TemplateItem item : mTemplateItemList)
                        if (item.getPhotoItemList().size() == selectedTemplateItem.getPhotoItemList().size()) {
                            tmp.add(item);
                        }
                    intent.putExtra(EXTRA_SELECTED_TEMPLATE_INDEX, tmp.indexOf(selectedTemplateItem));
                } else {
                    intent.putExtra(EXTRA_SELECTED_TEMPLATE_INDEX, mSelectedTemplateIndex);
                }
                ArrayList<String> imagePaths = new ArrayList<>();
                for (PhotoItem item : selectedTemplateItem.getPhotoItemList()) {
                    if (item.imagePath == null) item.imagePath = "";
                    imagePaths.add(item.imagePath);
                }
                intent.putExtra(EXTRA_IMAGE_PATHS, imagePaths);
                startActivity(intent);
            } catch (Exception ex) {
                ex.printStackTrace();
//                FirebaseCrash.report(ex);
            }
        }
    }
}
