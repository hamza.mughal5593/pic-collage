package com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import io.paperdb.Paper;

public class HuziLoadShowInter {

    private static HuziLoadShowInter myInstance = null;
    private com.facebook.ads.InterstitialAd myFbInter;
    private int Error = 0;
    private int Error_COUNT = 2;

    private static final String TAG = "Google_Fb_ads";
    private Context mContext;
    private com.google.android.gms.ads.interstitial.InterstitialAd myInterstitial;
    private Intent whereWantToGo = null;
    String adsMob_id_globle;
    String fb_id_globle;

    public static HuziLoadShowInter getInstance(Context mcontext) {
        if (myInstance == null) {
            myInstance = new HuziLoadShowInter(mcontext);
        }
        return myInstance;
    }

    public HuziLoadShowInter(Context mcontext) {
        this.mContext = mcontext;
        Log.e(TAG, "Call HUZI_GoogleAds class");
    }

    private String getReason(int errorCode) {
        String errorReason = "";
        switch (errorCode) {
            case AdRequest.ERROR_CODE_INTERNAL_ERROR:
                errorReason = "Internal_error";
                break;
            case AdRequest.ERROR_CODE_INVALID_REQUEST:
                errorReason = "Invalid_request_error";
                break;
            case AdRequest.ERROR_CODE_NETWORK_ERROR:
                errorReason = "Network_Problem_Error";
                break;
            case AdRequest.ERROR_CODE_NO_FILL:
                errorReason = "Noting_Show";
                break;
        }
        return errorReason;
    }


    public void loadInterstitial(Context context, String adsMob_id, String fb_id) {
        //  whereWantToGo=where;
        adsMob_id_globle = adsMob_id;
        fb_id_globle = fb_id;
/*
        if (!VD_AppController.isPurchaseApp) {
*/
//        if (Utils.isInternetAvailable(context)) {
//            if (Inter_AD_Controller.isGoogleonPeriorty) {
//                loadGoogleAd(context, adsMob_id, fb_id);
//            } else {
//                loadFBAd(context, adsMob_id, fb_id);
//            }
//        }
          /*
        }*/
    }


    private void loadGoogleAd(final Context ctx, final String google_ids, final String facebook_ids) {


        boolean inapp = Paper.book().read("inapp", false);
        if (inapp) {
            return;
        }

        if (Inter_AD_Controller.isgoogleSwitchEnabled) {

//            if (myInterstitial == null) {
//                Log.e(TAG, "Google Ad null");
//                myInterstitial = new InterstitialAd(ctx);
//                myInterstitial.setAdUnitId(google_ids);
//            }
            if (myInterstitial!=null) {
                Log.e(TAG, "Google Ad Already loaded");
            } else {


                com.google.android.gms.ads.interstitial.InterstitialAd.load(ctx, google_ids, new AdRequest.Builder().build(), new InterstitialAdLoadCallback() {
                    @Override
                    public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
                        myInterstitial = interstitialAd;
                        Error = 0;
                        Log.e(TAG, "google Ad Loaded");


                    }

                    @Override
                    public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
                        // Handle the error

                        myInterstitial = null;

                        String message = "onAdFailedToLoad: " + getReason(loadAdError.getCode());
                        Log.e(TAG, message);

                        Error = Error + 1;
                        if (Error <= Error_COUNT) {
                            loadFBAd(ctx, google_ids, facebook_ids);
                        }
                    }
                });

            }

        } else {
            loadFBAd(ctx, google_ids, facebook_ids);
        }
    }


    public void loadFBAd(final Context ctx, final String google_ids, final String facebook_ids) {


        boolean inapp = Paper.book().read("inapp", false);
        if (inapp) {
            return;
        }

        if (Inter_AD_Controller.isFbAdSwitchEnabled) {
            if (myFbInter == null) {
                myFbInter = new com.facebook.ads.InterstitialAd(ctx, facebook_ids);
            }

            if (myFbInter.isAdLoaded()) {
                Log.e(TAG, "myFbInter is Already loaded");
            }
            // Set listeners for the Interstitial Ad
            else {
                myFbInter.setAdListener(new InterstitialAdListener() {
                    @Override
                    public void onInterstitialDisplayed(Ad ad) {
                        // Interstitial ad displayed callback
                        Log.e(TAG, "Now FB Interstitial  displayed.");

                    }

                    @Override
                    public void onInterstitialDismissed(Ad ad) {
                        // Interstitial dismissed callback
                        Log.e(TAG, "FB Interstitial  dismissed.");
                        loadFBAd(ctx, google_ids, facebook_ids);
//                        if (whereWantToGo != null){
//                            mContext.startActivity(whereWantToGo);
//                        }
                    }

                    @Override
                    public void onError(Ad ad, AdError adError) {
                        // Ad error callback
                        Log.e(TAG, "FB Interstitial  failed to load: " + adError.getErrorMessage());
                        Error = Error + 1;
                        if (Error <= Error_COUNT) {
                            loadGoogleAd(ctx, google_ids, facebook_ids);
                        }
                    }

                    @Override
                    public void onAdLoaded(Ad ad) {
                        // Interstitial ad is loaded and ready to be displayed
                        Error = 0;
                        Log.e(TAG, "FB Interstitial  is loaded and ready to  display!");
                        // Show the ad


                    }

                    @Override
                    public void onAdClicked(Ad ad) {
                        // Ad clicked callback
                        Log.d(TAG, "FB Interstitial clicked!");
                    }

                    @Override
                    public void onLoggingImpression(Ad ad) {
                        // Ad impression logged callback
                        Log.d(TAG, "FB Interstitial  impression logged!");
                    }
                });
                myFbInter.loadAd();
            }
        } else {
            loadGoogleAd(ctx, google_ids, facebook_ids);
        }
    }


    public void showGoogleAndFbAd(Intent where, final Activity activity_) {
        whereWantToGo = where;
        if (whereWantToGo != null) {
            mContext.startActivity(whereWantToGo);
        }
//        Log.e(TAG, "showMethod call");
//        if (myInterstitial != null) {
//            boolean inapp = Paper.book().read("inapp", false);
//            if (inapp) {
//                if (whereWantToGo != null)
//                    mContext.startActivity(whereWantToGo);
//                return;
//            }
//            myInterstitial.setFullScreenContentCallback(new FullScreenContentCallback() {
//                @Override
//                public void onAdDismissedFullScreenContent() {
//                    Log.e(TAG, "AdClosed");
//                    myInterstitial = null;
//                    loadGoogleAd(activity_, adsMob_id_globle, fb_id_globle);
//                }
//
//
//                @Override
//                public void onAdShowedFullScreenContent() {
//                    // Called when fullscreen content is shown.
//                    // Make sure to set your reference to null so you don't
//                    // show it a second time.
//
//                    Log.d(TAG, "The ad was shown.");
//                }
//            });
//            myInterstitial.show(activity_);
////            myInterstitial.show();
//            Log.e(TAG, "Google Ad Show");
//        } else if (myFbInter != null && myFbInter.isAdLoaded() && !myFbInter.isAdInvalidated()) {
//            boolean inapp = Paper.book().read("inapp", false);
//            if (inapp) {
//                if (whereWantToGo != null)
//                    mContext.startActivity(whereWantToGo);
//                return;
//            }
//            myFbInter.show();
//            Log.e(TAG, "FB Ad Show");
//        } else {
////            if (whereWantToGo != null)
////                mContext.startActivity(whereWantToGo);
//            Log.e(TAG, "Both not show");
//        }


    }

}
