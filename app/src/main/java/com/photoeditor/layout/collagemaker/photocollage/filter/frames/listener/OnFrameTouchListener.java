package com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener;

import android.view.MotionEvent;

public interface OnFrameTouchListener {
	public void onFrameTouch(MotionEvent event);
	public void onFrameDoubleClick(MotionEvent event);
}
