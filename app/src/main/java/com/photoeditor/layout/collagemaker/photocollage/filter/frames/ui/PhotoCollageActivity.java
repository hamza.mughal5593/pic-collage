package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener.OnChooseColorListener;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener.OnShareImageListener;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.BaseFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.PhotoCollageFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.SelectFrameFragment;

import static com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.PhotoCollageFragment.checksave;


public class PhotoCollageActivity extends BaseFragmentActivity implements
        OnShareImageListener, OnChooseColorListener {
    public static final int PHOTO_TYPE = 1;
    public static final int FRAME_TYPE = 2;
    public static final String EXTRA_CREATED_METHOD_TYPE = "methodType";

    private int mSelectedColor = Color.GREEN;
    private boolean mClickedShareButton = false;
    TextView home_btn;
    ImageView exit_btn;
//    @Override
//    protected void preCreateAdsHelper() {
//        mLoadedData = false;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photocollage);

//        Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
//        this.setSupportActionBar(toolbar);
//        ActionBar actionBar = this.getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setTitle(R.string.app_name);
//        }

        home_btn = findViewById(R.id.home_btn);
        exit_btn = findViewById(R.id.exit_btn);

        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checksave){
                    checksave = false;
                    finish();
                }else {
                    showEditDialog();
                }
            }
        });
        exit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checksave){
                    checksave = false;
                    finish();
                }else {
                    showEditDialog();
                }
            }
        });

        if (savedInstanceState == null) {
            BaseFragment fragment = null;
            int type = getIntent().getIntExtra(EXTRA_CREATED_METHOD_TYPE, PHOTO_TYPE);
            if (type == PHOTO_TYPE) {
                fragment = new PhotoCollageFragment();
            } else {
                fragment = new SelectFrameFragment();
            }

            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, fragment)
                    .commit();
        } else {
            mClickedShareButton = savedInstanceState.getBoolean("mClickedShareButton", false);
        }
    }
    private void showEditDialog() {

        final Dialog dialog = new Dialog(PhotoCollageActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_alert_dialog);

        final TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        final TextView restart_btn = (TextView) dialog.findViewById(R.id.restart_btn);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checksave = false;
                dialog.dismiss();
                finish();

            }
        });
        dialog.show();

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("mClickedShareButton", mClickedShareButton);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mClickedShareButton) {
            mClickedShareButton = false;
//            if (getAdsHelper() != null) {
//                getAdsHelper().showInterstitialAds();
//            }
        }
    }

    @Override
    public void onBackPressed() {
        BaseFragment fragment = (BaseFragment) getVisibleFragment();
        if (fragment instanceof PhotoCollageFragment || fragment instanceof SelectFrameFragment) {

            if (checksave){
                checksave = false;
                finish();
            }else {
                showEditDialog();
            }


        } else {
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager.popBackStack();
        }
    }

    @Override
    public void onShareImage(String imagePath) {
        mClickedShareButton = true;
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        return fragmentManager.findFragmentById(R.id.frame_container);
    }

    @Override
    public void onShareFrame(String imagePath) {
        // TODO Auto-generated method stub
        Toast.makeText(this, "Shared image frame: " + imagePath,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void setSelectedColor(int color) {
        mSelectedColor = color;
    }

    @Override
    public int getSelectedColor() {
        return mSelectedColor;
    }
}

