package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.FragmentManager;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.TemplateItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.template.ItemImageView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.template.PhotoItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.template.PhotoLayout;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.custom.TransitionImageView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import java.io.File;

import collagemaker.photoeditor.ui.activity.ImageProcessingActivity;
import collagemaker.photoeditor.utils.FileUtils;
import collagemaker.photoeditor.utils.PhotoUtils;


public class TemplateDetailActivity extends BaseTemplateDetailActivity implements PhotoLayout.OnQuickActionClickListener {
    private PhotoLayout mPhotoLayout;
    private ItemImageView mSelectedItemImageView;
    private TransitionImageView mBackgroundImageView;
    private int mBackgroundColor = Color.BLACK;

    ImageView stickers_icon,text_icon,template_icon,save_btn;
    TextView sticker_text,text_text,template_text;



    RelativeLayout ad_main;
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    @SuppressLint("MissingPermission")
    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {

                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);

                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        com.google.android.gms.ads.nativead.NativeAdView adView3 = (com.google.android.gms.ads.nativead.NativeAdView) getLayoutInflater().inflate(R.layout.nativebanner, null);
                        Utils.BannerUnifiedNativeAdView(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();

//                        Toast.makeText(SplashActivity.this, errorCode+"", Toast.LENGTH_SHORT).show();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Toast.makeText(MainActivity.this, "Native ad failed to load: " + adError.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                Utils.fb_inflateAd(TemplateDetailActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
//                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        for (PhotoItem item : mSelectedTemplateItem.getPhotoItemList())
            if (item.imagePath != null && item.imagePath.length() > 0) {
                mSelectedPhotoPaths.add(item.imagePath);
            }
        ad_main = findViewById(R.id.ad_main);
        nativead = findViewById(R.id.nativead);
        fb_main = findViewById(R.id.fb_main);
        Boolean status = Utils.isInternetAvailable(this);

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            refreshAd();
        } else {
            ad_main.setVisibility(View.GONE);
        }

        template_icon = findViewById(R.id.template_icon);
        template_text = findViewById(R.id.template_text);
        stickers_icon = findViewById(R.id.sticker_icon);
        sticker_text = findViewById(R.id.sticker_text);
        text_icon = findViewById(R.id.text_icon);
        text_text = findViewById(R.id.text_text);
        save_btn = findViewById(R.id.save_btn);


        template_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mTemplateView.setVisibility(View.VISIBLE);


                template_icon.setImageResource(R.drawable.temp_red);
                template_text.setTextColor(getResources().getColor(R.color.red_text));

                stickers_icon.setImageResource(R.drawable.stickers_icon);
                sticker_text.setTextColor(getResources().getColor(R.color.textColor));
                text_icon.setImageResource(R.drawable.text_icon);
                text_text.setTextColor(getResources().getColor(R.color.textColor));
            }
        });
        stickers_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mTemplateView.setVisibility(View.GONE);

                stickers_icon.setImageResource(R.drawable.sticker_red_icon);
                sticker_text.setTextColor(getResources().getColor(R.color.red_text));

                template_icon.setImageResource(R.drawable.template);
                template_text.setTextColor(getResources().getColor(R.color.textColor));
                text_icon.setImageResource(R.drawable.text_icon);
                text_text.setTextColor(getResources().getColor(R.color.textColor));


                pickSticker();

            }
        });
        text_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mTemplateView.setVisibility(View.GONE);

                text_icon.setImageResource(R.drawable.text_red_icon);
                text_text.setTextColor(getResources().getColor(R.color.red_text));

                stickers_icon.setImageResource(R.drawable.stickers_icon);
                sticker_text.setTextColor(getResources().getColor(R.color.textColor));
                template_icon.setImageResource(R.drawable.template);
                template_text.setTextColor(getResources().getColor(R.color.textColor));


                FragmentManager fm = getSupportFragmentManager();
                DialogFragment picker = new AddTextItemActivity(null, 0, null, new AddTextItemActivity.CustomDialogInterface() {
                    @Override
                    public void okButtonClicked(String text, int mTextColor, String mFontPath) {
                        resultAddTextItem(text, mTextColor, mFontPath);
                    }
                });
                picker.show(fm, "Date Picker");

            }
        });
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                asyncSaveAndShare();
            }
        });

        // show guide on first time
//        boolean show = mPreferences.getBoolean(Constant.SHOW_GUIDE_CREATE_TEMPLATE_KEY, true);
//        if (show) {
//            clickInfoView();
//            mPreferences.edit().putBoolean(Constant.SHOW_GUIDE_CREATE_TEMPLATE_KEY, false)
//                    .commit();
//        }
    }
    @Override
    public void onBackPressed() {

        showEditDialog();
    }
    private void showEditDialog() {

        final Dialog dialog = new Dialog(TemplateDetailActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(collagemaker.photoeditor.R.layout.payment_alert_dialog);

        final TextView cancel = (TextView) dialog.findViewById(collagemaker.photoeditor.R.id.cancel);
        final TextView restart_btn = (TextView) dialog.findViewById(collagemaker.photoeditor.R.id.restart_btn);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                finish();

            }
        });
        dialog.show();

    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_template_detail;
    }

    @Override
    public Bitmap createOutputImage() {
        Bitmap template = mPhotoLayout.createImage();
        Bitmap result = Bitmap.createBitmap(template.getWidth(), template.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        canvas.drawBitmap(template, 0, 0, paint);
        template.recycle();
        template = null;
        Bitmap stickers = mPhotoView.getImage(mOutputScale);
        canvas.drawBitmap(stickers, 0, 0, paint);
        stickers.recycle();
        stickers = null;
        System.gc();
        return result;
    }

    @Override
    public void onEditActionClick(ItemImageView v) {
        mSelectedItemImageView = v;
        if (v.getImage() != null && v.getPhotoItem().imagePath != null && v.getPhotoItem().imagePath.length() > 0) {
            Uri uri = Uri.fromFile(new File(v.getPhotoItem().imagePath));
            requestEditingImage(uri);
        }
    }

    @Override
    public void onChangeActionClick(ItemImageView v) {
        mSelectedItemImageView = v;
        final Dialog dialog = getBackgroundImageDialog();
        if (dialog != null)
            dialog.findViewById(R.id.alterBackgroundView).setVisibility(View.GONE);
        requestPhoto();
    }

    @Override
    public void onChangeBackgroundActionClick(TransitionImageView v) {
        mBackgroundImageView = v;
        final Dialog dialog = getBackgroundImageDialog();
        if (dialog != null)
            dialog.findViewById(R.id.alterBackgroundView).setVisibility(View.VISIBLE);
        requestPhoto();
    }

    @Override
    protected void resultEditImage(Uri uri) {
        if (mBackgroundImageView != null) {
            mBackgroundImageView.setImagePath(FileUtils.getPath(this, uri));
            mBackgroundImageView = null;
        } else if (mSelectedItemImageView != null) {
            mSelectedItemImageView.setImagePath(FileUtils.getPath(this, uri));
        }
    }

    @Override
    protected void resultFromPhotoEditor(Uri image) {
        if (mBackgroundImageView != null) {
            mBackgroundImageView.setImagePath(FileUtils.getPath(this, image));
            mBackgroundImageView = null;
        } else if (mSelectedItemImageView != null) {
            mSelectedItemImageView.setImagePath(FileUtils.getPath(this, image));
        }
    }

    @Override
    protected void resultBackground(Uri uri) {
        if (mBackgroundImageView != null) {
            mBackgroundImageView.setImagePath(FileUtils.getPath(this, uri));
            mBackgroundImageView = null;
        } else if (mSelectedItemImageView != null) {
            mSelectedItemImageView.setImagePath(FileUtils.getPath(this, uri));
        }
    }

    @Override
    protected void buildLayout(TemplateItem templateItem) {
        Bitmap backgroundImage = null;
        if (mPhotoLayout != null) {
            backgroundImage = mPhotoLayout.getBackgroundImage();
            mPhotoLayout.recycleImages(false);
        }

        final Bitmap frameImage = PhotoUtils.decodePNGImage(this, templateItem.getTemplate());
        int[] size = calculateThumbnailSize(frameImage.getWidth(), frameImage.getHeight());
        //Photo Item item_quick_action must be descended by index before creating photo layout.
        mPhotoLayout = new PhotoLayout(this, templateItem.getPhotoItemList(), frameImage);
        mPhotoLayout.setBackgroundImage(backgroundImage);
//        mPhotoLayout.setBackgroundColor(getResources().getColor(R.color.bg_color));
        mContainerLayout.setBackgroundColor(mBackgroundColor);
        mPhotoLayout.setQuickActionClickListener(this);
        mPhotoLayout.build(size[0], size[1], mOutputScale);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(size[0], size[1]);
        params.addRule(RelativeLayout.CENTER_IN_PARENT);
        mContainerLayout.removeAllViews();
        mContainerLayout.addView(mPhotoLayout, params);
        //add sticker view
        mContainerLayout.removeView(mPhotoView);
        mContainerLayout.addView(mPhotoView, params);
    }

    @Override
    public void finish() {
        if (mPhotoLayout != null) {
            mPhotoLayout.recycleImages(true);
        }
        super.finish();
    }
}
