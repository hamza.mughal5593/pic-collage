package com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener;

import collagemaker.photoeditor.model.ItemPackageInfo;


public interface OnDownloadedPackageClickListener {
    void onDeleteButtonClick(int position, ItemPackageInfo info);

    void onItemClick(int position, ItemPackageInfo info);
}
