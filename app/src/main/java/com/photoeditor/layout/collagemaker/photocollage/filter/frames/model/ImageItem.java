package com.photoeditor.layout.collagemaker.photocollage.filter.frames.model;

public class ImageItem {
	public String imagePath;
	public String thumbnailPath;
	public boolean isSelected = false;
	public boolean isSticker = false;
}
