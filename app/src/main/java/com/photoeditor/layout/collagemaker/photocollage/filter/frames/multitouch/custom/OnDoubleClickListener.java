package com.photoeditor.layout.collagemaker.photocollage.filter.frames.multitouch.custom;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.multitouch.controller.MultiTouchEntity;

public interface OnDoubleClickListener {
	public void onPhotoViewDoubleClick(PhotoView view, MultiTouchEntity entity);
	public void onBackgroundDoubleClick();
}
