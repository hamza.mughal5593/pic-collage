package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.PermissionUtil;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import java.util.ArrayList;
import java.util.Random;

import io.paperdb.Paper;

public class SplashActivity extends AppCompatActivity {
    private ProgressBar pgsBar;
    private int i = 0;
    private Handler hdlr = new Handler();
    TextView progrestext;

    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    RelativeLayout ad_main;
    ProgressBar progressBar;
    TextView tv_loading;

    private com.google.android.gms.ads.interstitial.InterstitialAd mInterstitialAd;
    public com.facebook.ads.InterstitialAd fb_interstitial;
    AdRequest adReques;

    String TAG = "new_interstitial_load";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Paper.init(this);


//        boolean inapp = Paper.book().read("inapp", false);
//        if (!inapp) {
//            fb_interstitial = new com.facebook.ads.InterstitialAd(SplashActivity.this, getString(R.string.facebook_interstitial_one_SPLASH));
//
//            adReques = new AdRequest.Builder().build();
////            mInterstitialAd.loadAd(adReques);
//
//            com.google.android.gms.ads.interstitial.InterstitialAd.load(this, getString(R.string.InterstitialAd_id_SPLASH), adReques, new InterstitialAdLoadCallback() {
//                @Override
//                public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
//                    // The mInterstitialAd reference will be null until
//                    // an ad is loaded.
//                    mInterstitialAd = interstitialAd;
//                    Log.i(TAG, "onAdLoaded");
//
//
//
//
//                }
//
//                @Override
//                public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
//                    // Handle the error
//                    Log.i(TAG, loadAdError.getMessage());
//                    mInterstitialAd = null;
//
//                    fb_interstitial.loadAd();
//                }
//            });
//
//
//        }
//
//
//        ad_main = findViewById(R.id.ad_stars);
//        nativead = findViewById(R.id.nativead);
//        fb_main = findViewById(R.id.fb_main);
//        progressBar = findViewById(R.id.progressbar);
//        tv_loading = findViewById(R.id.tv_loading);
//        Boolean status = Utils.isInternetAvailable(this);
//
//        if (status) {
//            ad_main.setVisibility(View.VISIBLE);
//            nativead.setVisibility(View.VISIBLE);
//            refreshAd();
//        } else {
//            ad_main.setVisibility(View.INVISIBLE);
//            fb_main.setVisibility(View.GONE);
//            nativead.setVisibility(View.GONE);
//        }


        pgsBar = (ProgressBar) findViewById(R.id.pBar);
        progrestext = (TextView) findViewById(R.id.progrestext);


        i = pgsBar.getProgress();
        new Thread(new Runnable() {
            public void run() {
                while (i < 100) {
                    i += 1;
                    // Update the progress bar and display the current value in text view
                    hdlr.post(new Runnable() {
                        public void run() {
                            pgsBar.setProgress(i);
                            progrestext.setText(i + "%");
                            if (i == 100) {
//                                if (mInterstitialAd != null) {
//                                    mInterstitialAd.setFullScreenContentCallback(new FullScreenContentCallback() {
//                                        @Override
//                                        public void onAdDismissedFullScreenContent() {
//                                            Log.e(TAG, "AdClosed");
//                                            mInterstitialAd = null;
//                                            PermissionUtil.ask(SplashActivity.this);
//                                        }
//
//
//                                        @Override
//                                        public void onAdShowedFullScreenContent() {
//                                            // Called when fullscreen content is shown.
//                                            // Make sure to set your reference to null so you don't
//                                            // show it a second time.
//
//                                            Log.d(TAG, "The ad was shown.");
//                                        }
//                                    });
//                                }

//                                if (!check_native || !check_inter) {
                                next_screen();
//                                    check_native = false;
//                                    check_inter = false;
//                                if (mInterstitialAd != null) {
//                                    mInterstitialAd.show(SplashActivity.this);
//                                } else if (fb_interstitial != null) {
//                                    if (fb_interstitial.isAdLoaded()) {
//
//                                        fb_interstitial.show();
//                                    }
//                                }
//                                }

//                                fb_interstitial.setAdListener(new InterstitialAdListener() {
//                                    @Override
//                                    public void onInterstitialDisplayed(Ad ad) {
//                                        // Interstitial ad displayed callback
//                                    }
//
//                                    @Override
//                                    public void onInterstitialDismissed(Ad ad) {
//                                        // Interstitial dismissed callback
////                                            next_screen();
//                                    }
//
//                                    @Override
//                                    public void onError(Ad ad, com.facebook.ads.AdError adError) {
//                                        // Ad error callback
//
//                                    }
//
//                                    @Override
//                                    public void onAdLoaded(Ad ad) {
//                                        // Interstitial ad is loaded and ready to be displayed
//                                        // Show the ad
////                                        check_inter = true;
////                                        if (check_native && check_inter) {
////                                            next_screen();
////
////                                            if (mInterstitialAd != null) {
////                                                mInterstitialAd.show(SplashActivity.this);
////                                            } else if (fb_interstitial != null) {
////                                                if (fb_interstitial.isAdLoaded()) {
////
////                                                    fb_interstitial.show();
////                                                }
////                                            }
////                                        }
//
//                                    }
//
//                                    @Override
//                                    public void onAdClicked(Ad ad) {
//                                        // Ad clicked callback
//
//                                    }
//
//                                    @Override
//                                    public void onLoggingImpression(Ad ad) {
//                                        // Ad impression logged callback
//                                    }
//                                });


                            }
                        }
                    });
                    try {
                        // Sleep for 100 milliseconds to show the progress slowly.
                        Thread.sleep(30);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }

    void next_screen() {


        Intent intent = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(intent);
        PermissionUtil.ask(SplashActivity.this);
        finish();


    }

    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance_splash))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {
                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tv_loading.setVisibility(View.GONE);
                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        NativeAdView adView3 = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ad_splash, null);
                        Utils.populateFullAdavanceNative_Splash(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                        // Show the ad.
                    }
                })
//                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
//                    @Override
//                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//
//
//                    }
//                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();
                        super.onAdFailedToLoad(loadAdError);
                    }

                    @Override
                    public void onAdImpression() {
//                        check_native = true;
//
//                        if (check_native && check_inter) {
//                            next_screen();
//
//                            if (mInterstitialAd != null) {
//                                mInterstitialAd.show(SplashActivity.this);
//                            } else if (fb_interstitial != null) {
//                                if (fb_interstitial.isAdLoaded()) {
//                                    fb_interstitial.show();
//                                }
//                            }
//                        }
                        super.onAdImpression();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb_splash));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }

                progressBar.setVisibility(View.GONE);
                tv_loading.setVisibility(View.GONE);
                // Inflate Native Ad into Container
                Utils.fb_full_nativeadvance_for_splash(SplashActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//                check_native = true;
//
//                if (check_native && check_inter) {
//                    next_screen();
//
//                    if (mInterstitialAd != null) {
//                        mInterstitialAd.show(SplashActivity.this);
//                    } else if (fb_interstitial != null) {
//                        if (fb_interstitial.isAdLoaded()) {
//                            fb_interstitial.show();
//                        }
//                    }
//                }
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }
}
