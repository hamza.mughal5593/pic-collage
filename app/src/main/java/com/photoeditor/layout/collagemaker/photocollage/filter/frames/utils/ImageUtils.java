package com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.widget.ImageView;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.config.ALog;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import collagemaker.photoeditor.utils.DateTimeUtils;
import collagemaker.photoeditor.utils.PhotoUtils;

public class ImageUtils {
    public static class MemoryInfo {
        public long availMem = 0;
        public long totalMem = 0;
    }

    public static final String OUTPUT_COLLAGE_FOLDER = Environment.getExternalStorageDirectory().toString()+ "/DCIM/PicCollage";
    private static final float MIN_OUTPUT_IMAGE_SIZE = 640.0f;

    public static void loadImageWithPicasso(final Context context, final ImageView imageView, final String uri) {
        if (uri != null && uri.length() > 1) {
            if (uri.startsWith("http://") || uri.startsWith("https://")) {
                Picasso.with(context).load(uri).into(imageView);
            } else if (uri.startsWith(PhotoUtils.DRAWABLE_PREFIX)) {
                try {
                    int id = Integer.parseInt(uri.substring(PhotoUtils.DRAWABLE_PREFIX.length()));
                    Picasso.with(context).load(id).into(imageView);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            } else if (uri.startsWith(PhotoUtils.ASSET_PREFIX)) {
                String file = uri.substring(PhotoUtils.ASSET_PREFIX.length());
                Picasso.with(context).load(Uri.parse("file:///android_asset/".concat(file))).into(imageView);
            } else {
                Picasso.with(context).load(new File(uri)).into(imageView);
            }
        }
    }

    public static MemoryInfo getMemoryInfo(Context context) {
        final MemoryInfo info = new MemoryInfo();
        ActivityManager actManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        ActivityManager.MemoryInfo memInfo = new ActivityManager.MemoryInfo();
        actManager.getMemoryInfo(memInfo);
        info.availMem = memInfo.availMem;

        if (Build.VERSION.SDK_INT >= 16) {
            info.totalMem = memInfo.totalMem;
        } else {
            try {
                final RandomAccessFile reader = new RandomAccessFile("/proc/meminfo", "r");
                final String load = reader.readLine();
                // Get the Number value from the string
                Pattern p = Pattern.compile("(\\d+)");
                Matcher m = p.matcher(load);
                String value = "";
                while (m.find()) {
                    value = m.group(1);
                }
                reader.close();
                info.totalMem = (long) Double.parseDouble(value) * 1024;
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        ALog.d("ImageUtils", "getMemoryInfo, availMem=" + info.availMem + ", totalMem=" + info.totalMem);
        return info;
    }

    public static float calculateOutputScaleFactor(int viewWidth, int viewHeight) {
        float ratio = Math.min(viewWidth, viewHeight) / MIN_OUTPUT_IMAGE_SIZE;
        if (ratio < 1 && ratio > 0) {
            ratio = 1.0f / ratio;
        } else {
            ratio = 1;
        }
        ALog.d("ImageUtils", "calculateOutputScaleFactor, viewWidth=" + viewWidth + ", viewHeight=" + viewHeight + ", ratio=" + ratio);
        return ratio;
    }



    public static float pxFromDp(final Context context, final float dp) {
        if (context.getResources()!=null){

            return dp * context.getResources().getDisplayMetrics().density;
        }else return 0;
    }

    public static Matrix createMatrixToDrawImageInCenterView(final float viewWidth, final float viewHeight, final float imageWidth, final float imageHeight) {
        final float ratioWidth = ((float) viewWidth) / imageWidth;
        final float ratioHeight = ((float) viewHeight) / imageHeight;
        final float ratio = Math.max(ratioWidth, ratioHeight);
        final float dx = (viewWidth - imageWidth) / 2.0f;
        final float dy = (viewHeight - imageHeight) / 2.0f;
        Matrix result = new Matrix();
        result.postTranslate(dx, dy);
        result.postScale(ratio, ratio, viewWidth / 2, viewHeight / 2);
        return result;
    }

    public static long getUsedMemorySize() {

        long freeSize = 0L;
        long totalSize = 0L;
        long usedSize = -1L;
        try {
            Runtime info = Runtime.getRuntime();
            freeSize = info.freeMemory();
            totalSize = info.totalMemory();
            usedSize = totalSize - freeSize;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usedSize;

    }

    public static void recycleView(View iv) {
        if (iv == null) {
            return;
        }

        Drawable background = iv.getBackground();
        iv.setBackgroundColor(Color.TRANSPARENT);

        if (background != null && background instanceof BitmapDrawable) {
            Bitmap bm = ((BitmapDrawable) background).getBitmap();
            if (bm != null && !bm.isRecycled()) {
                bm.recycle();
                bm = null;
            }
        }
    }


    /**
     * @param v
     * @return
     * @throws OutOfMemoryError
     * @deprecated
     */
    public static Bitmap loadBitmapFromView(View v) throws OutOfMemoryError {
        try {
            final int width = v.getMeasuredWidth();
            final int height = v.getMeasuredHeight();
            final Drawable bg = v.getBackground();
            v.setBackgroundColor(Color.TRANSPARENT);
            v.layout(0, 0, width, height);
            Bitmap returnedBitmap = Bitmap.createBitmap(width, height,
                    Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(returnedBitmap);
            if (bg != null) {
                bg.draw(c);
            }

            v.draw(c);
            if (Build.VERSION.SDK_INT >= 16) {
                v.setBackground(bg);
            } else {
                v.setBackgroundDrawable(bg);
            }

            return returnedBitmap;
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }
    }

    /**
     * @param view
     * @param outputImagePath
     * @throws OutOfMemoryError
     * @deprecated
     */
    public static void takeScreen(View view, final String outputImagePath)
            throws OutOfMemoryError {
        try {
            Bitmap bitmap = loadBitmapFromView(view);
            File imageFile = new File(outputImagePath);
            imageFile.getParentFile().mkdirs();
            OutputStream fout = null;

            fout = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fout);
            fout.flush();
            fout.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
            throw err;
        }
    }

    public static void saveBitmap(Bitmap bitmap, final String path) {
        OutputStream fout = null;
        try {
            fout = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, fout);
            fout.flush();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                fout.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static int getOrientationFromExif(String imagePath) {
        int orientation = -1;
        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            // StringBuilder builder = new StringBuilder();
            //
            // builder.append("Date & Time: " +
            // getExifTag(exif,ExifInterface.TAG_DATETIME) + "\n\n");
            // builder.append("Flash: " +
            // getExifTag(exif,ExifInterface.TAG_FLASH) + "\n");
            // builder.append("Focal Length: " +
            // getExifTag(exif,ExifInterface.TAG_FOCAL_LENGTH) + "\n\n");
            // builder.append("GPS Datestamp: " +
            // getExifTag(exif,ExifInterface.TAG_FLASH) + "\n");
            // builder.append("GPS Latitude: " +
            // getExifTag(exif,ExifInterface.TAG_GPS_LATITUDE) + "\n");
            // builder.append("GPS Latitude Ref: " +
            // getExifTag(exif,ExifInterface.TAG_GPS_LATITUDE_REF) + "\n");
            // builder.append("GPS Longitude: " +
            // getExifTag(exif,ExifInterface.TAG_GPS_LONGITUDE) + "\n");
            // builder.append("GPS Longitude Ref: " +
            // getExifTag(exif,ExifInterface.TAG_GPS_LONGITUDE_REF) + "\n");
            // builder.append("GPS Processing Method: " +
            // getExifTag(exif,ExifInterface.TAG_GPS_PROCESSING_METHOD) + "\n");
            // builder.append("GPS Timestamp: " +
            // getExifTag(exif,ExifInterface.TAG_GPS_TIMESTAMP) + "\n\n");
            // builder.append("Image Length: " +
            // getExifTag(exif,ExifInterface.TAG_IMAGE_LENGTH) + "\n");
            // builder.append("Image Width: " +
            // getExifTag(exif,ExifInterface.TAG_IMAGE_WIDTH) + "\n\n");
            // builder.append("Camera Make: " +
            // getExifTag(exif,ExifInterface.TAG_MAKE) + "\n");
            // builder.append("Camera Model: " +
            // getExifTag(exif,ExifInterface.TAG_MODEL) + "\n");
            // builder.append("Camera Orientation: " +
            // getExifTag(exif,ExifInterface.TAG_ORIENTATION) + "\n");
            // builder.append("Camera White Balance: " +
            // getExifTag(exif,ExifInterface.TAG_WHITE_BALANCE) + "\n");
            // builder.append("Camera orientation=" + getExifTag(exif,
            // ExifInterface.TAG_ORIENTATION));
            // ALog.d("ImageUtils.getOrientationFromExif", "exif=" +
            // builder.toString());
            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    orientation = 270;

                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    orientation = 180;

                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    orientation = 90;

                    break;

                case ExifInterface.ORIENTATION_NORMAL:
                    orientation = 0;

                    break;
                default:
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return orientation;
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {

        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Config.ARGB_8888);

        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        float r = 0;

        if (bitmap.getWidth() > bitmap.getHeight()) {
            r = bitmap.getHeight() / 2;
        } else {
            r = bitmap.getWidth() / 2;
        }

        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        paint.setDither(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2, r,
                paint);
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

}
