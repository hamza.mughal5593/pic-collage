package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter.SelectedPhotoAdapter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.BaseFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.DownloadedPackageFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.GalleryAlbumImageFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;
//import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.File;
import java.util.ArrayList;

import collagemaker.photoeditor.database.table.ItemPackageTable;


public class DownloadedPackageActivity extends BaseFragmentActivity implements SelectedPhotoAdapter.OnDeleteButtonClickListener,
        GalleryAlbumImageFragment.OnSelectImageListener {
    public static final String EXTRA_IMAGE_COUNT = "imageCount";
    public static final String EXTRA_IS_MAX_IMAGE_COUNT = "isMaxImageCount";
    public static final String EXTRA_SELECTED_IMAGES = "selectedImages";

    private RecyclerView mRecyclerView;
    private TextView mImageCountView;

    private ArrayList<String> mSelectedImages = new ArrayList<>();
    private SelectedPhotoAdapter mSelectedPhotoAdapter;
    private int mNeededImageCount = 0;
    private boolean mIsMaxImageCount = false;
    //Showing messages
    private String mFormattedText;
    private String mFormattedWarningText;
    private String mPackageType = ItemPackageTable.BACKGROUND_TYPE;
ImageView next_btn;


    RelativeLayout ad_main;
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    @SuppressLint("MissingPermission")
    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {

                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);

                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        com.google.android.gms.ads.nativead.NativeAdView adView3 = (com.google.android.gms.ads.nativead.NativeAdView) getLayoutInflater().inflate(R.layout.nativebanner, null);
                        Utils.BannerUnifiedNativeAdView(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                    }
                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {

                    @Override
                    public void onAdFailedToLoad(int errorCode) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();

//                        Toast.makeText(SplashActivity.this, errorCode+"", Toast.LENGTH_SHORT).show();
                        // Handle the failure by logging, altering the UI, and so on.
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Toast.makeText(MainActivity.this, "Native ad failed to load: " + adError.getErrorMessage(), Toast.LENGTH_SHORT).show();
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }
                // Inflate Native Ad into Container
                Utils.fb_inflateAd(DownloadedPackageActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
                // Native ad impression
//                Log.d(TAG, "Native ad impression logged!");
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }


    @SuppressLint("StringFormatInvalid")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_photo);



        ad_main = findViewById(R.id.ad_main);
        nativead = findViewById(R.id.nativead);
        fb_main = findViewById(R.id.fb_main);
        Boolean status = Utils.isInternetAvailable(this);

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            refreshAd();
        } else {
            ad_main.setVisibility(View.GONE);
        }


        mNeededImageCount = getIntent().getIntExtra(EXTRA_IMAGE_COUNT, 0);
        mIsMaxImageCount = getIntent().getBooleanExtra(EXTRA_IS_MAX_IMAGE_COUNT, false);
        mPackageType = getIntent().getStringExtra(DownloadedPackageFragment.EXTRA_PACKAGE_TYPE);
        //show data
        if (ItemPackageTable.BACKGROUND_TYPE.equals(mPackageType)) {
            mNeededImageCount = 1;
            mIsMaxImageCount = false;
        } else {
            mNeededImageCount = BaseFragment.MAX_NEEDED_PHOTOS;
            mIsMaxImageCount = true;
        }
        next_btn = findViewById(R.id.next_btn);
//        addNativeAdView();

        mRecyclerView = (RecyclerView) findViewById(R.id.selectedImageRecyclerView);
        mImageCountView = (TextView) findViewById(R.id.imageCountView);
        //Warning messages
        if (!mIsMaxImageCount) {
            mFormattedText = String.format(getString(R.string.please_select_photo), mNeededImageCount);
        } else {
            mFormattedText = getString(R.string.please_select_photo_without_counting);
        }
        mImageCountView.setText(mFormattedText.concat("(0)"));
        mFormattedWarningText = String.format(getString(R.string.you_need_photo), mNeededImageCount);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        mSelectedPhotoAdapter = new SelectedPhotoAdapter(mSelectedImages, this);
        if (ItemPackageTable.STICKER_TYPE.equals(mPackageType)) {
            mSelectedPhotoAdapter.setImageFitCenter(true);
        } else {
            mSelectedPhotoAdapter.setImageFitCenter(false);
        }

        mRecyclerView.setAdapter(mSelectedPhotoAdapter);

        DownloadedPackageFragment fragment = new DownloadedPackageFragment();
        Bundle bundle = new Bundle();
        bundle.putString(DownloadedPackageFragment.EXTRA_PACKAGE_TYPE, mPackageType);
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction().replace(R.id.frame_container, fragment).commit();


        next_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mSelectedImages.size() < mNeededImageCount && !mIsMaxImageCount) {
                    Toast.makeText(DownloadedPackageActivity.this, mFormattedText, Toast.LENGTH_SHORT).show();
                } else {
                    Intent data = new Intent();
                    data.putExtra(EXTRA_SELECTED_IMAGES, mSelectedImages);
                    setResult(RESULT_OK, data);
                    //log
                    if (mSelectedImages != null && mSelectedImages.size() > 0) {
                        for (String str : mSelectedImages) {
                            if (mPackageType != null && str != null && mPackageType.length() > 0 && str.length() > 0) {
                                String msg = mPackageType.concat("/");
                                File file = new File(str);
                                if (file.getParentFile() != null) {
                                    msg = msg.concat(file.getParentFile().getName());
                                }
//                                msg = msg.concat("_").concat(file.getName());
//                                Bundle bundle = new Bundle();
//                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, msg);
//                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, str);
//                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
//                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
                            }
                        }
                    }
                    //show ads
//                if (getAdsHelper() != null)
//                    getAdsHelper().showInterstitialAds();
                    //finish
                    finish();
                }
            }
        });

    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_gallery, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        if (item.getItemId() == R.id.action_done) {
//            if (mSelectedImages.size() < mNeededImageCount && !mIsMaxImageCount) {
//                Toast.makeText(this, mFormattedText, Toast.LENGTH_SHORT).show();
//            } else {
//                Intent data = new Intent();
//                data.putExtra(EXTRA_SELECTED_IMAGES, mSelectedImages);
//                setResult(RESULT_OK, data);
//                //log
//                if (mSelectedImages != null && mSelectedImages.size() > 0) {
//                    for (String str : mSelectedImages) {
//                        if (mPackageType != null && str != null && mPackageType.length() > 0 && str.length() > 0) {
//                            String msg = mPackageType.concat("/");
//                            File file = new File(str);
//                            if (file.getParentFile() != null) {
//                                msg = msg.concat(file.getParentFile().getName());
//                            }
//                            msg = msg.concat("_").concat(file.getName());
//                            Bundle bundle = new Bundle();
////                            bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, msg);
////                            bundle.putString(FirebaseAnalytics.Param.ITEM_ID, str);
////                            bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, str);
////                            mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
//                        }
//                    }
//                }
//                //show ads
////                if (getAdsHelper() != null)
////                    getAdsHelper().showInterstitialAds();
//                //finish
//                finish();
//            }
//            return true;
//        } else {
//            return super.onOptionsItemSelected(item);
//        }
//    }

    @Override
    public void onBackPressed() {
        finish();
//        Fragment fragment = getFragmentManager().findFragmentById(R.id.frame_container);
//        if (fragment != null && fragment instanceof DownloadedPackageFragment) {
//            super.onBackPressed();
//        } else {
////            getFragmentManager().popBackStack();
////            super.onBackPressed();
//            finish();
//        }
    }

    @Override
    public void onDeleteButtonClick(String image) {
        mSelectedImages.remove(image);
        mSelectedPhotoAdapter.notifyDataSetChanged();
        mImageCountView.setText(mFormattedText.concat("(" + mSelectedImages.size() + ")"));
    }

    @Override
    public void onSelectImage(String image) {
        if (mSelectedImages.size() == mNeededImageCount) {
            Toast.makeText(this, mFormattedWarningText, Toast.LENGTH_SHORT).show();
        } else {
            mSelectedImages.remove(image);
            mSelectedImages.add(image);
            mSelectedPhotoAdapter.notifyDataSetChanged();
            mImageCountView.setText(mFormattedText.concat("(" + mSelectedImages.size() + ")"));
        }
    }
}
