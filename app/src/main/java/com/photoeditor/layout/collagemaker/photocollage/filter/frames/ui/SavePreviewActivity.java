package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads.HuziConstants;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads.HuziLoadShowInter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ImageUtils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;

import collagemaker.photoeditor.utils.DateTimeUtils;
import collagemaker.photoeditor.utils.PhotoUtils;
import io.paperdb.Paper;

public class SavePreviewActivity extends AppCompatActivity {
    ImageView show_image, save_btn,home_btn,back_btn;
boolean checksave_pre=false;

    HuziLoadShowInter envy_inter;
    String google_ad_ids, fb_ad_ids;
    private void loadInterFirst() {

        google_ad_ids = getString(R.string.InterstitialAd_id);
        fb_ad_ids = getString(R.string.facebook_interstitial_one);

        envy_inter.loadInterstitial(SavePreviewActivity.this, google_ad_ids, fb_ad_ids);
    }
    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    RelativeLayout ad_main;
    ProgressBar progressBar;
    TextView tv_loading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_preview);



        ad_main = findViewById(R.id.ad_stars);
        nativead = findViewById(R.id.nativead);
        fb_main = findViewById(R.id.fb_main);
        progressBar = findViewById(R.id.progressbar);
        tv_loading = findViewById(R.id.tv_loading);
        Boolean status = Utils.isInternetAvailable(this);

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            nativead.setVisibility(View.VISIBLE);
            refreshAd();
        } else {
            ad_main.setVisibility(View.GONE);
            fb_main.setVisibility(View.GONE);
            nativead.setVisibility(View.GONE);
        }


        envy_inter = HuziLoadShowInter.getInstance(SavePreviewActivity.this);
        loadInterFirst();




        byte[] byteArray = Paper.book().read("image_bitmap",null);
        Bitmap bmp = null;
        if (byteArray!=null){

            bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
        }

        show_image = findViewById(R.id.show_image);
        save_btn = findViewById(R.id.save_btn);
        home_btn = findViewById(R.id.home_btn);
        back_btn = findViewById(R.id.back_btn);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (checksave_pre){
//
//                    checksave_pre = false;
//                    finish();
//
//                }else {
//                    showEditDialog();
//
//                }
                finish();
            }
        });

        show_image.setImageBitmap(bmp);


        final Bitmap finalBmp = bmp;
        save_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                checksave_pre = true;
                asyncSaveAndShare(finalBmp);
                HuziConstants.show_inter_ad(SavePreviewActivity.this, null);
            }
        });
        home_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                if (checksave_pre){

                    checksave_pre = false;
                    Intent intent = new Intent(SavePreviewActivity.this,MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                }else {
                    showEditDialog();

                }
            }
        });

    }




    private void refreshAd() {
        AdLoader adLoader = new AdLoader.Builder(this, getString(R.string.native_advance))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {
                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tv_loading.setVisibility(View.GONE);
                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = findViewById(R.id.fl_adplaceholder3);
                        NativeAdView adView3 = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ad_splash, null);
                        Utils.populateFullAdavanceNative_Splash(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                        // Show the ad.
                    }
                })
//                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
//                    @Override
//                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//
//
//                    }
//                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd();
                        super.onAdFailedToLoad(loadAdError);
                    }

                    @Override
                    public void onAdImpression() {
//                        check_native = true;
//
//                        if (check_native && check_inter) {
//                            next_screen();
//
//                            if (mInterstitialAd != null) {
//                                mInterstitialAd.show(SplashActivity.this);
//                            } else if (fb_interstitial != null) {
//                                if (fb_interstitial.isAdLoaded()) {
//                                    fb_interstitial.show();
//                                }
//                            }
//                        }
                        super.onAdImpression();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd() {

        final NativeAdLayout nativeAdLayout = findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(this, getString(R.string.native_advance_fb));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }

                progressBar.setVisibility(View.GONE);
                tv_loading.setVisibility(View.GONE);
                // Inflate Native Ad into Container
                Utils.fb_full_nativeadvance_for_splash(SavePreviewActivity.this, fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//                check_native = true;
//
//                if (check_native && check_inter) {
//                    next_screen();
//
//                    if (mInterstitialAd != null) {
//                        mInterstitialAd.show(SplashActivity.this);
//                    } else if (fb_interstitial != null) {
//                        if (fb_interstitial.isAdLoaded()) {
//                            fb_interstitial.show();
//                        }
//                    }
//                }
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }



    private void showEditDialog() {

        final Dialog dialog = new Dialog(SavePreviewActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_alert_dialog);

        final TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        final TextView restart_btn = (TextView) dialog.findViewById(R.id.restart_btn);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checksave_pre = true;
                dialog.dismiss();

                Intent intent = new Intent(SavePreviewActivity.this,MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);

            }
        });
        dialog.show();

    }
    public void asyncSaveAndShare(Bitmap bmp) {

        AsyncTask<Bitmap, Void, File> task = new AsyncTask<Bitmap, Void, File>() {
            Dialog dialog;
            String errMsg;



            @Override
            public void onPreExecute() {
                super.onPreExecute();
                dialog = ProgressDialog.show(SavePreviewActivity.this, getString(R.string.app_name), getString(R.string.creating));
            }

            @Override
            protected File doInBackground(Bitmap... params) {
                try {

                    Bitmap image = params[0];
//                    final String folderPath = getApplicationContext().getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS)+"/checkfolder";

                    String fileName = DateTimeUtils.getCurrentDateTime().replaceAll(":", "-").concat(".png");
                    File collageFolder = new File(ImageUtils.OUTPUT_COLLAGE_FOLDER);
//                    File collageFolder = new File(folderPath);
                    if (!collageFolder.exists()) {
                        collageFolder.mkdirs();
                    }
                    File photoFile = new File(collageFolder, fileName);
                    image.compress(Bitmap.CompressFormat.PNG, 100, new FileOutputStream(photoFile));

                    PhotoUtils.addImageToGallery(photoFile.getAbsolutePath(), SavePreviewActivity.this);
                    return null;
                } catch (Exception ex) {
                    ex.printStackTrace();
                    errMsg = ex.getMessage();
                } catch (OutOfMemoryError err) {
                    err.printStackTrace();
                    errMsg = err.getMessage();
//                    FirebaseCrash.report(err);
                }
                return null;
            }

            @Override
            protected void onPostExecute(File file) {
                super.onPostExecute(file);
                try {
                    save_btn.setImageResource(R.drawable.save_green);
                    dialog.dismiss();
                    Toast.makeText(SavePreviewActivity.this, "Saved to internal Storage Successfully", Toast.LENGTH_LONG).show();
                    save_btn.setClickable(false);
//                    finish();
                } catch (Exception ex) {
                    if (errMsg != null) {
                        Toast.makeText(SavePreviewActivity.this, errMsg, Toast.LENGTH_LONG).show();
                    }
                    ex.printStackTrace();
                }

//                if (file != null) {
//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("image/png");
//                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
//                    startActivity(Intent.createChooser(share, getString(R.string.photo_editor_share_image)));
//                } else if (errMsg != null) {
//                    Toast.makeText(BaseTemplateDetailActivity.this, errMsg, Toast.LENGTH_LONG).show();
//                }
                //log
//                Bundle bundle = new Bundle();
//                if (mIsFrameImage) {
//                    String[] layoutRatioName = new String[]{"square", "fit", "golden"};
//                    String ratio = "";
//                    if (mLayoutRatio < layoutRatioName.length)
//                        ratio = layoutRatioName[mLayoutRatio];
////                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "share/frame_".concat(ratio).concat("_").concat(mSelectedTemplateItem.getTitle()));
//                } else {
////                    bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "share/template_".concat(mSelectedTemplateItem.getTitle()));
//                }

//                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, mSelectedTemplateItem.getTitle());
//                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            }
        };
        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR,bmp);
    }

}