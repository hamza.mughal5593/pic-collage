package com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.notifications;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.BuildConfig;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.SplashActivity;

import java.util.Map;

public class QG_NotificationHelper extends ContextWrapper {
    private static final String chanlle_id = BuildConfig.APPLICATION_ID;
    private static final String chanlle_name = "SEEKS APPS";
    NotificationManager manager;
    Bitmap logo;

    public QG_NotificationHelper(Context base) {
        super(base);
        creatc_chaneels();
    }

    @TargetApi(Build.VERSION_CODES.O)
    private void creatc_chaneels() {
        NotificationChannel notificationChannel = new NotificationChannel(chanlle_id,
                chanlle_name, NotificationManager.IMPORTANCE_DEFAULT);
        notificationChannel.enableLights(true);
        notificationChannel.enableVibration(true);
        notificationChannel.setLightColor(Color.GREEN);
        notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        getManager().createNotificationChannel(notificationChannel);
    }

    public NotificationManager getManager() {
        if (manager == null)
            manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        return manager;

    }


    @TargetApi(Build.VERSION_CODES.O)
    public void getnotifictaion(Map<String, String> data) {



        String title = null,short_desc = null,long_desc = null;
        if (data!=null && !data.isEmpty()){
            title = data.get("title");
            short_desc = data.get("short_desc");
            long_desc = data.get("long_desc");
        }


        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                1, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        Uri notification_sound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Notification.Builder builder = new Notification.Builder(getApplicationContext(),
                chanlle_id)
                .setContentTitle(title)
                .setContentText(short_desc)
                .setStyle(new Notification.BigTextStyle()
                        .bigText(long_desc))
                .setSmallIcon(R.drawable.app_icon)
                .setSound(notification_sound)
                .setContentIntent(pendingIntent)
                .setAutoCancel(false);
        getManager().notify(1, builder.build());
    }

}
