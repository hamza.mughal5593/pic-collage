package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ImageDecoder;

import java.io.File;

import collagemaker.photoeditor.utils.DialogUtils;
import collagemaker.photoeditor.utils.PhotoUtils;
import collagemaker.photoeditor.utils.Utils;


public class ViewImageActivity extends AppCompatActivity {
    public static final String IMAGE_FILE_KEY = "imageFile";

    private View mActionLayout;
    private View mShareView;
    private View mEditView;
    private View mDeleteView;
    private ImageView mImageView;
    private String mImagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_image);
        mImagePath = getIntent().getStringExtra(IMAGE_FILE_KEY);
        // Inflate widgets
        mActionLayout = findViewById(R.id.actionLayout);
        mShareView = findViewById(R.id.shareView);
        mShareView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (mImagePath != null && mImagePath.length() > 0) {
                    //postPhoto(mImagePath);
                    String whiteImage = mImagePath;//.substring(0, mImagePath.length() - 4).concat(PhotoUtils.EDITED_WHITE_IMAGE_SUFFIX);


                    File newFile = new File(whiteImage);
                    Uri contentUri = FileProvider.getUriForFile(ViewImageActivity.this, "com.photoeditor.layout.collagemaker.photocollage.filter.frames.provider", newFile);

                    if (contentUri != null) {
                        Intent shareIntent = new Intent();
                        shareIntent.setAction(Intent.ACTION_SEND);
                        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); // temp permission for receiving app to read this file
                        shareIntent.setDataAndType(contentUri, getContentResolver().getType(contentUri));
                        shareIntent.putExtra(Intent.EXTRA_STREAM, contentUri);
                        startActivity(Intent.createChooser(shareIntent, "Choose an app"));
                    }


//                    Intent share = new Intent(Intent.ACTION_SEND);
//                    share.setType("image/jpeg");
//                    share.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(new File(whiteImage)));
//                    startActivity(Intent.createChooser(share, getString( R.string.photo_editor_share_image)));
                }
            }
        });

//		mEditView = findViewById(R.id.editView);
//		mEditView.setOnClickListener(new View.OnClickListener() {
//
//			@Override
//			public void onClick(View v) {
//				Intent i = new Intent(ViewImageActivity.this, ImageProcessingActivity.class);
//			    Uri imageUri = Uri.fromFile(new File(mImagePath));
//				i.putExtra(ImageProcessingActivity.IMAGE_URI_KEY, imageUri);
//				i.putExtra(ImageProcessingActivity.IS_EDITING_IMAGE_KEY, true);
//				startActivity(i);
//				finish();
//			}
//		});

        mDeleteView = findViewById(R.id.deleteView);
        mDeleteView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
//                File file = new File(mImagePath);
//                file.delete();
//						File thumbnail = new File(Utils.EDITED_IMAGE_THUMBNAIL_FOLDER, file.getName());
//						thumbnail.delete();
//						file = new File(mImagePath.substring(0, mImagePath.length() - 4).concat(PhotoUtils.EDITED_WHITE_IMAGE_SUFFIX));
//						file.delete();
//                finish();
//                DialogUtils.showCoolConfirmDialog(ViewImageActivity.this, R.string.photo_editor_app_name, R.string.photo_editor_confirm_delete_image,
//                        new DialogUtils.ConfirmDialogOnClickListener() {
//                            @Override
//                            public void onOKButtonOnClick() {
//
//                            }
//
//                            @Override
//                            public void onCancelButtonOnClick() {
//
//                            }
//                        });
                showEditDialog();
            }
        });

        mImageView = (ImageView) findViewById(R.id.imageView);
        if (mImagePath != null) {
            mImageView.setImageBitmap(ImageDecoder.decodeFileToBitmap(mImagePath));
        }

        findViewById(R.id.backButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void showEditDialog() {

        final Dialog dialog = new Dialog(ViewImageActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_alert_dialog);

        final TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        final TextView restart_btn = (TextView) dialog.findViewById(R.id.restart_btn);
        final TextView cat_name = (TextView) dialog.findViewById(R.id.cat_name);
        cat_name.setText("Are you sure you want to delete?");


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                File file = new File(mImagePath);
                file.delete();
                File thumbnail = new File(Utils.EDITED_IMAGE_THUMBNAIL_FOLDER, file.getName());
                thumbnail.delete();
                file = new File(mImagePath.substring(0, mImagePath.length() - 4).concat(PhotoUtils.EDITED_WHITE_IMAGE_SUFFIX));
                file.delete();

                dialog.dismiss();
                finish();

            }
        });
        dialog.show();

    }
}