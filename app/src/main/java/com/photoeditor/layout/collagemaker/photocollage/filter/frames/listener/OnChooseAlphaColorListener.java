package com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener;

public interface OnChooseAlphaColorListener {
	public void onChooseColor(int color);
}
