package com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener;

public interface OnChooseColorListener {
	public void setSelectedColor(int color);

	public int getSelectedColor();
}
