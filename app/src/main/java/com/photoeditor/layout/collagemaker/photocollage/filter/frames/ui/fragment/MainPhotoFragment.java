package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.NativeAd;
import com.facebook.ads.NativeAdLayout;
import com.facebook.ads.NativeAdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads.HuziConstants;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.Ads.HuziLoadShowInter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.PhotoCollageActivity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.SavedActivity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.SplashActivity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.TemplateActivity;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.PermissionUtil;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.Utils;

import io.paperdb.Paper;


public class MainPhotoFragment extends BaseFragment {

    HuziLoadShowInter envy_inter;
    String google_ad_ids, fb_ad_ids;

    private void loadInterFirst() {

        google_ad_ids = getString(R.string.InterstitialAd_id);
        fb_ad_ids = getString(R.string.facebook_interstitial_one);

        envy_inter.loadInterstitial(getActivity(), google_ad_ids, fb_ad_ids);
    }

    LinearLayout nativead;
    private com.google.android.gms.ads.nativead.NativeAd native_Ad;
    private NativeAd fb_nativeAd;
    ScrollView fb_main;
    RelativeLayout ad_main;
    ProgressBar progressBar;
    TextView tv_loading;


    private void refreshAd(final View rootView) {
        AdLoader adLoader = new AdLoader.Builder(getActivity(), getString(R.string.native_advance_splash))
                .forNativeAd(new com.google.android.gms.ads.nativead.NativeAd.OnNativeAdLoadedListener() {
                    @Override
                    public void onNativeAdLoaded(com.google.android.gms.ads.nativead.NativeAd nativeAd) {
                        fb_main.setVisibility(View.GONE);
                        nativead.setVisibility(View.VISIBLE);
                        progressBar.setVisibility(View.GONE);
                        tv_loading.setVisibility(View.GONE);
                        if (native_Ad != null) {
                            native_Ad.destroy();
                        }
                        native_Ad = nativeAd;
                        FrameLayout frameLayout3 = rootView.findViewById(R.id.fl_adplaceholder3);
                        NativeAdView adView3 = (NativeAdView) getLayoutInflater().inflate(R.layout.native_ad_splash, null);
                        Utils.populateFullAdavanceNative_Splash(native_Ad, adView3);

                        frameLayout3.removeAllViews();


                        frameLayout3.addView(adView3);
                        // Show the ad.
                    }
                })
//                .forUnifiedNativeAd(new UnifiedNativeAd.OnUnifiedNativeAdLoadedListener() {
//                    @Override
//                    public void onUnifiedNativeAdLoaded(UnifiedNativeAd unifiedNativeAd) {
//
//
//                    }
//                })
                .withAdListener(new com.google.android.gms.ads.AdListener() {
                    @Override
                    public void onAdFailedToLoad(LoadAdError loadAdError) {
                        fb_main.setVisibility(View.VISIBLE);
                        nativead.setVisibility(View.GONE);
                        fb_load_NativeAd(rootView);
                        super.onAdFailedToLoad(loadAdError);
                    }

                    @Override
                    public void onAdImpression() {
//                        check_native = true;
//
//                        if (check_native && check_inter) {
//                            next_screen();
//
//                            if (mInterstitialAd != null) {
//                                mInterstitialAd.show(SplashActivity.this);
//                            } else if (fb_interstitial != null) {
//                                if (fb_interstitial.isAdLoaded()) {
//                                    fb_interstitial.show();
//                                }
//                            }
//                        }
                        super.onAdImpression();
                    }
                })
                .withNativeAdOptions(new NativeAdOptions.Builder()
                        // Methods in the NativeAdOptions.Builder class can be
                        // used here to specify individual options settings.
                        .build())
                .build();

        adLoader.loadAd(new AdRequest.Builder().build());
    }

    private void fb_load_NativeAd(View rootView) {

        final NativeAdLayout nativeAdLayout = rootView.findViewById(R.id.native_ad_container);

        fb_nativeAd = new NativeAd(getActivity(), getString(R.string.native_advance_fb_splash));
        fb_nativeAd.setAdListener(new NativeAdListener() {
            @Override
            public void onMediaDownloaded(Ad ad) {
                // Native ad finished downloading all assets
//                Log.e(TAG, "Native ad finished downloading all assets.");
            }

            @Override
            public void onError(Ad ad, AdError adError) {
                // Native ad failed to load
//                Log.e(TAG, "Native ad failed to load: " + adError.getErrorMessage());
            }

            @Override
            public void onAdLoaded(Ad ad) {
                // Race condition, load() called again before last ad was displayed
                if (fb_nativeAd == null || fb_nativeAd != ad) {
                    return;
                }

                progressBar.setVisibility(View.GONE);
                tv_loading.setVisibility(View.GONE);
                // Inflate Native Ad into Container
                Utils.fb_full_nativeadvance_for_splash(getActivity(), fb_nativeAd, nativeAdLayout);
            }

            @Override
            public void onAdClicked(Ad ad) {
                // Native ad clicked
//                Log.d(TAG, "Native ad clicked!");
            }

            @Override
            public void onLoggingImpression(Ad ad) {
//                check_native = true;
//
//                if (check_native && check_inter) {
//                    next_screen();
//
//                    if (mInterstitialAd != null) {
//                        mInterstitialAd.show(SplashActivity.this);
//                    } else if (fb_interstitial != null) {
//                        if (fb_interstitial.isAdLoaded()) {
//                            fb_interstitial.show();
//                        }
//                    }
//                }
            }
        });

        // Request an ad
        fb_nativeAd.loadAd();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_photo, null);

        Paper.init(getActivity());


        ad_main = rootView.findViewById(R.id.ad_stars);
        nativead = rootView.findViewById(R.id.nativead);
        fb_main = rootView.findViewById(R.id.fb_main);
        progressBar = rootView.findViewById(R.id.progressbar);
        tv_loading = rootView.findViewById(R.id.tv_loading);
        Boolean status = Utils.isInternetAvailable(getActivity());

        if (status) {
            ad_main.setVisibility(View.VISIBLE);
            nativead.setVisibility(View.VISIBLE);
            refreshAd(rootView);
        } else {
            ad_main.setVisibility(View.GONE);
            fb_main.setVisibility(View.GONE);
            nativead.setVisibility(View.GONE);
        }


        envy_inter = HuziLoadShowInter.getInstance(getActivity());
        loadInterFirst();

        View photoView = rootView.findViewById(R.id.photoButton);
        photoView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Paper.book().write("single_frame", false);

                createFromPhoto();
            }
        });
        View edit_single_frame = rootView.findViewById(R.id.edit_single_frame);
        edit_single_frame.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {

                Paper.book().write("single_frame", true);

                createFromPhoto();
            }
        });

        View frameView = rootView.findViewById(R.id.frameButton);
        frameView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                createFromFrame();
            }
        });

        rootView.findViewById(R.id.imageTemplateButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                createFromTemplate();
            }
        });
        rootView.findViewById(R.id.saved_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), SavedActivity.class);
                HuziConstants.show_inter_ad(getActivity(), intent);
            }
        });


        return rootView;
    }

    @Override
    protected void setTitle() {
        String mTitle = getString(R.string.home);
        setTitle(mTitle);
    }


    public void createFromPhoto() {


        if (!already()) {
            return;
        }
        Intent intent = new Intent(getActivity(), PhotoCollageActivity.class);
        intent.putExtra(PhotoCollageActivity.EXTRA_CREATED_METHOD_TYPE, PhotoCollageActivity.PHOTO_TYPE);
        startActivity(intent);
//        HuziConstants.show_inter_ad(getActivity(), intent);
        PermissionUtil.ask(getActivity());

    }

    public void createFromFrame() {

        if (!already()) {
            return;
        }
        Intent intent = new Intent(getActivity(), TemplateActivity.class);
        intent.putExtra(TemplateActivity.EXTRA_IS_FRAME_IMAGE, true);
        HuziConstants.show_inter_ad(getActivity(), intent);
        PermissionUtil.ask(getActivity());
    }

    public void createFromTemplate() {

        if (!already()) {
            return;
        }
        Intent intent = new Intent(getActivity(), TemplateActivity.class);
        intent.putExtra(PhotoCollageActivity.EXTRA_CREATED_METHOD_TYPE, PhotoCollageActivity.FRAME_TYPE);
        HuziConstants.show_inter_ad(getActivity(), intent);
        PermissionUtil.ask(getActivity());
    }


}
