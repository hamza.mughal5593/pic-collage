package com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.notifications;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.SplashActivity;

import java.util.Map;

public class QG_MyFirebaseMessagingService extends FirebaseMessagingService {
    public static int NOTIFICATION_ID = 1;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            QG_NotificationHelper notificationHelper = new QG_NotificationHelper(getBaseContext());
            notificationHelper.getnotifictaion(remoteMessage.getData());
        } else {
            sendNotification(remoteMessage.getData());
        }
    }

    private void sendNotification(Map<String, String> data) {



        SharedPreferences mPrefs = getSharedPreferences("NotificationBadgeCount", Context.MODE_PRIVATE);
        int count = mPrefs.getInt("count", 0);
        count = count + 1;

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt("count", count);
        editor.apply();



        int num = ++NOTIFICATION_ID;


        String title = null,short_desc = null,long_desc = null;
        if (data!=null && !data.isEmpty()){
            title = data.get("title");
            short_desc = data.get("short_desc");

        }



        Intent intent = new Intent(this, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, num , intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle(title)
                .setContentText(short_desc)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(num, notificationBuilder.build());
    }

}
