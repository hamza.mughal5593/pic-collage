package com.photoeditor.layout.collagemaker.photocollage.filter.frames.config;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.BuildConfig;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.PhotoCollageApp;

/**
 * Config all debug options in application. In release version, all value must
 * set to FALSE
 */
public class DebugOptions {
    public static final boolean ENABLE_LOG = BuildConfig.DEBUG;

    public static final boolean ENABLE_DEBUG = BuildConfig.DEBUG;

    public static final boolean ENABLE_FOR_DEV = false;

    public static boolean isProVersion() {
        return collagemaker.photoeditor.config.DebugOptions.isProVersion(PhotoCollageApp.getAppContext());
    }
}
