package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment;

import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.Nullable;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter.GalleryAlbumAdapter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.config.ALog;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.GalleryAlbum;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.BaseFragmentActivity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

import collagemaker.photoeditor.utils.DateTimeUtils;


public class GalleryAlbumFragment extends BaseFragment {
    private ListView mListView;
    private View mProgressBar;
    private ArrayList<GalleryAlbum> mAlbums;
    private GalleryAlbumAdapter mAdapter;

    private Parcelable mListViewState;


    @Override
    public void onPause() {
        // Save ListView mListViewState @ onPause
        if (mListView != null)
            mListViewState = mListView.onSaveInstanceState();
        super.onPause();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gallery_album, container, false);
        mListView = (ListView) view.findViewById(R.id.listView);
        mProgressBar = view.findViewById(R.id.progressBar);

        AsyncTask<Void, Void, ArrayList<GalleryAlbum>> task = new AsyncTask<Void, Void, ArrayList<GalleryAlbum>>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected ArrayList<GalleryAlbum> doInBackground(Void... params) {
                ArrayList<GalleryAlbum> result = loadPhotoAlbums();
                return result;
            }

            @Override
            protected void onPostExecute(ArrayList<GalleryAlbum> galleryAlbums) {
                super.onPostExecute(galleryAlbums);
                if (already()) {
                    mProgressBar.setVisibility(View.GONE);
                    mAlbums = galleryAlbums;
                    if (mAlbums != null) {
                        if (mAlbums.size()!=0){
                            Collections.sort(mAlbums, new Comparator<GalleryAlbum>() {
                                public int compare(GalleryAlbum v1, GalleryAlbum v2) {
                                    return v1.getAlbumName().compareTo(v2.getAlbumName());
                                }
                            });
                        }

                    }
                    mAdapter = new GalleryAlbumAdapter(getActivity(), mAlbums, new GalleryAlbumAdapter.OnGalleryAlbumClickListener() {
                        @Override
                        public void onGalleryAlbumClick(GalleryAlbum album) {
                            BaseFragmentActivity activity = (BaseFragmentActivity) getActivity();
                            Bundle data = new Bundle();
                            data.putStringArrayList(GalleryAlbumImageFragment.ALBUM_IMAGE_EXTRA, (ArrayList<String>) album.getImageList());
                            data.putString(GalleryAlbumImageFragment.ALBUM_NAME_EXTRA, album.getAlbumName());
                            GalleryAlbumImageFragment fragment = new GalleryAlbumImageFragment();
                            fragment.setArguments(data);
                            FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
                            ft.replace(R.id.frame_container, fragment);
                            ft.addToBackStack(null);
                            ft.commit();
                        }
                    });
//                    createAdmobAdapterWrapper();
                    mListView.setAdapter(mAdapter);
                    // Restore previous mListViewState (including selected item index and scroll position)
                    if (mListViewState != null) {
                        mListView.onRestoreInstanceState(mListViewState);
                    }
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);

        setTitle(R.string.gallery_albums);
        return view;
    }

    public ArrayList<GalleryAlbum> loadPhotoAlbums() {
        final HashMap<Long, GalleryAlbum> map = new HashMap<>();
        final String[] projection = new String[]{
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.BUCKET_ID,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATE_TAKEN
        };

        Uri images = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        Cursor cur = null;
        try {
            ContentResolver cr = getActivity().getContentResolver();
            cur = cr.query(images,
                    projection, // Which columns to return
                    "",         // Which rows to return (all rows)
                    null,       // Selection arguments (none)
                    ""          // Ordering
            );

            ALog.i("ListingImages", " query count=" + cur.getCount());

            if (cur != null && cur.moveToFirst()) {
                do {
                    String bucketName = cur.getString(cur.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME));
                    if (bucketName==null){
                        bucketName="";
                    }
                    long date = cur.getLong(cur.getColumnIndex(MediaStore.Images.Media.DATE_TAKEN));
                    String imagePath = cur.getString(cur.getColumnIndex(MediaStore.Images.Media.DATA));
                    long bucketId = cur.getLong(cur.getColumnIndex(MediaStore.Images.Media.BUCKET_ID));
                    GalleryAlbum album = map.get(bucketId);
                    if (album == null) {
                        album = new GalleryAlbum(bucketId, bucketName);
                        album.setTakenDate(DateTimeUtils.toUTCDateTimeString(new Date(date)));
                        album.getImageList().add(imagePath);
                        map.put(bucketId, album);
                    } else {
                        album.getImageList().add(imagePath);
                    }
                } while (cur.moveToNext());
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (cur != null)
                cur.close();
        }

        Collection<GalleryAlbum> albums = map.values();
        ArrayList<GalleryAlbum> result = new ArrayList<>();
        result.addAll(albums);
        return result;
    }
}
