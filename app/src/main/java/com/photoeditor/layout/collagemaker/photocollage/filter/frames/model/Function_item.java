package com.photoeditor.layout.collagemaker.photocollage.filter.frames.model;

public class Function_item {
    int icon_selected;
    int icon_unselected;
    String title;
    boolean isSelected;


    public Function_item(int icon_unselected, int icon_selected , String title, boolean isSelected) {
        this.icon_selected = icon_selected;
        this.icon_unselected = icon_unselected;
        this.title = title;
        this.isSelected = isSelected;
    }

    public int getIcon_selected() {
        return icon_selected;
    }

    public void setIcon_selected(int icon_selected) {
        this.icon_selected = icon_selected;
    }

    public int getIcon_unselected() {
        return icon_unselected;
    }

    public void setIcon_unselected(int icon_unselected) {
        this.icon_unselected = icon_unselected;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
