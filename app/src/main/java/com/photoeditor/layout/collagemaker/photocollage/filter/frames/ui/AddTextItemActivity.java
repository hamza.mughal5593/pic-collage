package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.adapter.FontAdapter;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.listener.OnChooseColorListener;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.model.FontItem;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.TextUtils;

import java.util.List;

import collagemaker.photoeditor.colorpicker.ColorPickerDialog;


public class AddTextItemActivity extends DialogFragment implements OnChooseColorListener, ColorPickerDialog.OnColorChangedListener {
    public String EXTRA_TEXT_CONTENT = "content";
    public static  String EXTRA_TEXT_COLOR = "color";
    public static  String EXTRA_TEXT_FONT = "font";

    private Spinner mFontSpinner;
    private View mColorView;
    private EditText mEditText;
    private ColorPickerDialog mColorPickerDialog;

    private List<FontItem> mFontItems;
    private int mTextColor = Color.BLACK;
    private String mFontPath;
    CustomDialogInterface customDI;
    String text;
    TextView ok_btn;
    public AddTextItemActivity(String EXTRA_TEXT_CONTENT, int EXTRA_TEXT_COLOR, String EXTRA_TEXT_FONT, CustomDialogInterface customDI) {
        this.customDI = customDI;
        this.text =EXTRA_TEXT_CONTENT;
        if (EXTRA_TEXT_COLOR!=0){
            this.mTextColor =EXTRA_TEXT_COLOR;
        }
        this.mFontPath =EXTRA_TEXT_FONT;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_add_text_item, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mColorView = view.findViewById(R.id.colorView);
        mColorView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mColorPickerDialog == null) {
                    mColorPickerDialog = new ColorPickerDialog(getActivity(), mTextColor);
                    mColorPickerDialog.setOnColorChangedListener(AddTextItemActivity.this);
                }

                mColorPickerDialog.setOldColor(mTextColor);
                if (!mColorPickerDialog.isShowing()) {
                    mColorPickerDialog.show();
                }
            }
        });

        mFontSpinner = (Spinner) view.findViewById(R.id.spinner);
        mEditText = (EditText) view.findViewById(R.id.editText);
        ok_btn = (TextView) view.findViewById(R.id.ok_btn);
        //In case edit text
//        mTextColor = getIntent().getIntExtra(EXTRA_TEXT_COLOR, mTextColor);
//        mFontPath = getIntent().getStringExtra(EXTRA_TEXT_FONT);
//        text = getIntent().getStringExtra(EXTRA_TEXT_CONTENT);
        mEditText.setTextColor(mTextColor);
        if (text != null && text.length() > 0) {
            mEditText.setText(text);
        }
        //set fonts
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                mFontItems = TextUtils.loadFonts(getActivity());
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                final FontAdapter fontAdapter = new FontAdapter(getActivity(), mFontItems);
                mFontSpinner.setAdapter(fontAdapter);
                mFontSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        mFontPath = mFontItems.get(position).getFontPath();
                        mEditText.setTypeface(TextUtils.loadTypeface(getActivity(), mFontPath));
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
                if (mFontPath != null && mFontPath.length() > 0) {
                    for (int idx = 0; idx < mFontItems.size(); idx++)
                        if (mFontItems.get(idx).getFontPath() != null && mFontItems.get(idx).getFontPath().equalsIgnoreCase(mFontPath)) {
                            mFontSpinner.setSelection(idx);
                            break;
                        }
                }
            }
        };

        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);


        ok_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickDoneButton();
            }
        });
    }

    public static interface CustomDialogInterface {

        // This is just a regular method so it can return something or
        // take arguments if you like.
        public void okButtonClicked(String text,int mTextColor,String mFontPath);


    }

    void setCustomDialogInterface(CustomDialogInterface customDialogInterface) {
        this.customDI = customDialogInterface;
    }
//    @Override
//    public void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_text_item);
//        final Toolbar toolbar = (Toolbar) this.findViewById(R.id.toolbar);
//        this.setSupportActionBar(toolbar);
//        final ActionBar actionBar = this.getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//            actionBar.setTitle(R.string.app_name);
//        }

//        addNativeAdView();

//    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_add_text_item, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == android.R.id.home) {
////            onBackPressed();
//            return true;
//        } else if (id == R.id.action_done) {
//            clickDoneButton();
//            return true;
//        } else {
//            return super.onOptionsItemSelected(item);
//        }
//    }

    public void clickDoneButton() {
        final String text = mEditText.getText().toString().trim();
//        Intent data = new Intent();
//        data.putExtra(EXTRA_TEXT_CONTENT, text);
//        data.putExtra(EXTRA_TEXT_COLOR, mTextColor);
//        data.putExtra(EXTRA_TEXT_FONT, mFontPath);
//        setResult(RESULT_OK, data);
//        finish();
        customDI.okButtonClicked(text,mTextColor,mFontPath);
        getDialog().dismiss();
    }

    @Override
    public void setSelectedColor(int color) {
        mTextColor = color;
        mEditText.setTextColor(mTextColor);
    }

    @Override
    public int getSelectedColor() {
        return mTextColor;
    }

    @Override
    public void onColorChanged(int color) {
        mTextColor = color;
        mEditText.setTextColor(mTextColor);
    }
}
