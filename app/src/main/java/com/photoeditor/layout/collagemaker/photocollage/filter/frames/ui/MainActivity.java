package com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui;

import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.billingclient.api.BillingClient;
import com.android.billingclient.api.BillingClientStateListener;
import com.android.billingclient.api.BillingFlowParams;
import com.android.billingclient.api.BillingResult;
import com.android.billingclient.api.Purchase;
import com.android.billingclient.api.PurchasesUpdatedListener;
import com.android.billingclient.api.SkuDetails;
import com.android.billingclient.api.SkuDetailsParams;
import com.android.billingclient.api.SkuDetailsResponseListener;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnSuccessListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.BuildConfig;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.R;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.config.ALog;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.config.DebugOptions;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.BaseFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.CreatedCollageFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.ui.fragment.MainPhotoFragment;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.DialogUtils;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.PermissionUtil;
import com.photoeditor.layout.collagemaker.photocollage.filter.frames.utils.ResultContainer;
//import com.google.firebase.crash.FirebaseCrash;

import java.util.Arrays;
import java.util.List;

import collagemaker.photoeditor.api.response.StoreItem;
import collagemaker.photoeditor.database.DatabaseManager;
import collagemaker.photoeditor.utils.StoreUtils;
import io.paperdb.Paper;

public class MainActivity extends BaseFragmentActivity {
    public static final String RATE_APP_PREF_NAME = "rateAppPref";
    public static final String RATED_APP_KEY = "ratedApp";
    public static final String OPEN_APP_COUNT_KEY = "openAppCount";

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerlayout;
    private String mTitle;
ImageView drawer_btn,remove_ad_btn;

    private BillingClient billingClient;


    private void inAppUpdateTranslator() {
        final AppUpdateManager appUpdateManagerForTrans = AppUpdateManagerFactory.create(MainActivity.this);
        Task<AppUpdateInfo> appUpdateInfoTaskForTrans = appUpdateManagerForTrans.getAppUpdateInfo();
        appUpdateInfoTaskForTrans.addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
            @Override
            public void onSuccess(AppUpdateInfo appUpdateInfo) {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                        && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                    try {
                        appUpdateManagerForTrans.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE
                                , MainActivity.this, 101);
                    } catch (IntentSender.SendIntentException e) {
                        e.printStackTrace();

//                    Log.d(TAG,"callInAppUpdate:"+e.getMessage());
                    }
                }
            }
        });
    }

    private FirebaseAnalytics mFirebaseAnalytics;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data == null) return;

        if (requestCode == 101) {
            Toast.makeText(this, "Downloading start...", Toast.LENGTH_SHORT).show();
            if (resultCode != RESULT_OK) {
//                Log.d(TAG,"onActivityResult: Update flow failed " + resultCode);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Paper.init(this);
        Paper.book().write("inapp", true);
        if (!DatabaseManager.getInstance(this).isDbFileExisted()) {
            DatabaseManager.getInstance(this).createDb();
        } else {
            boolean isOpen = DatabaseManager.getInstance(this).openDb();
            ALog.d("MainActivity", "onCreate, database isOpen=" + isOpen);
        }

        inAppUpdateTranslator();


        setupbillingclint();
        remove_ad_btn = findViewById(R.id.remove_ad_btn);
        remove_ad_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buyknow();
            }
        });




        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        if (savedInstanceState != null) {
            mTitle = savedInstanceState.getString("mTitle");
        }

        mDrawerlayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        final LinearLayout navigationView = findViewById(R.id.nav_view);

        drawer_btn = findViewById(R.id.drawer_btn);
        drawer_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (navigationView.getVisibility() == View.VISIBLE){
                    navigationView.setVisibility(View.GONE);
                }else {
                    navigationView.setVisibility(View.VISIBLE);
                }
                if (mDrawerlayout.isDrawerOpen(navigationView)) {
                    mDrawerlayout.closeDrawer(navigationView);
                } else {
                    mDrawerlayout.openDrawer(navigationView);
                }
            }
        });




        findViewById(R.id.remove).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                buyknow();

            }
        });



        findViewById(R.id.rate_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onRateAppButtonClick();

            }
        });
        findViewById(R.id.privacy_policy).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String url = "https://bespoketechltd.blogspot.com/2021/05/pic-collage-collage-maker-photo-editor.html";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);

            }
        });

        findViewById(R.id.share_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Pic Collage");
                    String shareMessage = "\nLet me recommend you this application\n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }


            }
        });
        findViewById(R.id.exit_app).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDrawerlayout.closeDrawer(GravityCompat.START);
                exitdialog();



            }
        });

        //set view
        if (savedInstanceState == null) {
            ResultContainer.getInstance().clearAll();
            getFragmentManager().beginTransaction()
                    .replace(R.id.frame_container, new MainPhotoFragment(), "MainPhotoFragment")
                    .commit();
        }


    }
    private void buyknow(){
        if (Paper.book().read("inapp", false)){

        }else {
            SkuDetailsParams params = SkuDetailsParams.newBuilder()
                    .setSkusList(Arrays.asList("collagemaker_removeads"))
                    .setType(BillingClient.SkuType.INAPP)
                    .build();

            billingClient.querySkuDetailsAsync(params, new SkuDetailsResponseListener() {
                @Override
                public void onSkuDetailsResponse(BillingResult billingResult, List<SkuDetails> list) {
                    if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {
//                            Toast.makeText(MainActivity.this, "" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();

                        BillingFlowParams billingFlowParams = BillingFlowParams.newBuilder()
                                .setSkuDetails(list.get(0))
                                .build();
                        billingClient.launchBillingFlow(MainActivity.this, billingFlowParams);
                    } else {
                        Toast.makeText(MainActivity.this, "Cannot Remove ads", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void exitdialog() {


        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.setContentView(R.layout.exit_from_app_dialog);





        TextView yes = (TextView) dialog.findViewById(R.id.yes);
        TextView no = (TextView) dialog.findViewById(R.id.no);


        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
                finishAffinity();
            }
        });


        dialog.show();
    }

    private void setupbillingclint() {

        billingClient = BillingClient.newBuilder(MainActivity.this).enablePendingPurchases().setListener(new PurchasesUpdatedListener() {
            @Override
            public void onPurchasesUpdated(@NonNull BillingResult billingResult, @Nullable List<Purchase> list) {

                if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.OK) {

                    Toast.makeText(MainActivity.this, "Successfully Remove ads", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
                    showEditDialog();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED){
                    Toast.makeText(MainActivity.this, "Purchases Restored...", Toast.LENGTH_LONG).show();
                    Paper.book().write("inapp", true);
                    showEditDialog();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.BILLING_UNAVAILABLE){
                    Toast.makeText(MainActivity.this, "Billing Unavailable..!", Toast.LENGTH_SHORT).show();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.ERROR){
                    Toast.makeText(MainActivity.this, "Billing Error..!", Toast.LENGTH_SHORT).show();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_DISCONNECTED){
                    Toast.makeText(MainActivity.this, "Service Disconnected..!", Toast.LENGTH_SHORT).show();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_TIMEOUT){
                    Toast.makeText(MainActivity.this, "Service Timeout..!", Toast.LENGTH_SHORT).show();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.SERVICE_UNAVAILABLE){
                    Toast.makeText(MainActivity.this, "Service Unavailable..!", Toast.LENGTH_SHORT).show();
                }else if (billingResult.getResponseCode() == BillingClient.BillingResponseCode.USER_CANCELED){
                    Toast.makeText(MainActivity.this, "Billing not Completed..!", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(MainActivity.this, "Billing Error..!", Toast.LENGTH_SHORT).show();
                }


            }
        }).build();

        billingClient.startConnection(new BillingClientStateListener() {
            @Override
            public void onBillingSetupFinished(BillingResult billingResult) {
                if (billingResult.getResponseCode() != BillingClient.BillingResponseCode.OK) {
//                    Toast.makeText(PremiumScreenActivity.this, "" + billingResult.getResponseCode(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onBillingServiceDisconnected() {


//                Toast.makeText(MainActivity.this, "You are disconnected from billing", Toast.LENGTH_SHORT).show();
            }
        });


    }
    private void showEditDialog() {

        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.payment_alert_dialog_buy);

        final TextView cancel = (TextView) dialog.findViewById(R.id.cancel);
        final TextView restart_btn = (TextView) dialog.findViewById(R.id.restart_btn);


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        restart_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

                Intent intent = new Intent(MainActivity.this, SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finishAffinity();
            }
        });
        dialog.show();

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("mTitle", mTitle);
    }


    public void onRateAppButtonClick() {
        try {
            final SharedPreferences preferences = getSharedPreferences(RATE_APP_PREF_NAME, Context.MODE_PRIVATE);
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse("market://details?id=" + getPackageName()));
            startActivity(i);
            // save result
            preferences.edit().putBoolean(RATED_APP_KEY, true).commit();
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(MainActivity.this, getString(R.string.app_not_found), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            BaseFragment fragment = (BaseFragment) getVisibleFragment();
            if (fragment instanceof MainPhotoFragment) {
                exitdialog();
            } else {

                getFragmentManager().beginTransaction()
                        .replace(R.id.frame_container, new MainPhotoFragment(), "MainPhotoFragment")
                        .commit();
            }
        }
    }

    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getFragmentManager();
        return fragmentManager.findFragmentById(R.id.frame_container);
    }
}
