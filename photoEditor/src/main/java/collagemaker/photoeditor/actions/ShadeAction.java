package collagemaker.photoeditor.actions;

import android.os.Bundle;
import collagemaker.photoeditor.database.table.ShadeTable;
import collagemaker.photoeditor.ui.activity.ImageProcessingActivity;

public class ShadeAction extends FrameAction {
	public ShadeAction(ImageProcessingActivity activity) {
		super(activity);
	}

	@Override
	public void saveInstanceState(Bundle bundle) {
		super.saveInstanceState(bundle);
		bundle.putInt("collagemaker.photoeditor.actions.ShadeAction.mCurrentPosition", mCurrentPosition);
		bundle.putLong("collagemaker.photoeditor.actions.ShadeAction.mPackageId", mCurrentPackageId);
	}

	@Override
	public void restoreInstanceState(Bundle bundle) {
		super.restoreInstanceState(bundle);
		mCurrentPosition = bundle.getInt("collagemaker.photoeditor.actions.ShadeAction.mCurrentPosition", mCurrentPosition);
		mCurrentPackageId = bundle.getLong("collagemaker.photoeditor.actions.ShadeAction.mCurrentPosition", mCurrentPackageId);
	}

	@Override
	protected String getShadeType() {
		return ShadeTable.SHADE_TYPE;
	}
}
