package collagemaker.photoeditor.listener;

import collagemaker.photoeditor.model.ItemPackageInfo;

public interface OnInstallStoreItemListener {
	public void onStartDownloading(ItemPackageInfo item);

	public void onFinishInstalling(ItemPackageInfo item, boolean update);
}
