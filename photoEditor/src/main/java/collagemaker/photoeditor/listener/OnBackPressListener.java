package collagemaker.photoeditor.listener;

public interface OnBackPressListener {
	public void onBackButtonPress();
}
