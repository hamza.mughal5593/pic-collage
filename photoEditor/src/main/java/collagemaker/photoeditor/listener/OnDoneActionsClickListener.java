package collagemaker.photoeditor.listener;

public interface OnDoneActionsClickListener {
	public void onDoneButtonClick();

	public void onApplyButtonClick();
}
