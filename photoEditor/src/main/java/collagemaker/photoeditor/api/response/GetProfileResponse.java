package collagemaker.photoeditor.api.response;


public class GetProfileResponse extends BaseResponse{
    private User mUser;

    public User getUser() {
        return mUser;
    }
}
