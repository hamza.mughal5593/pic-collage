package collagemaker.photoeditor.api.response;


public class GetBigDAdsResponse extends BaseResponse{
    private BigDAds[] mBigdAds;

    public BigDAds[] getBigdAds() {
        return mBigdAds;
    }
}
