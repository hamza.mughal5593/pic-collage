package collagemaker.photoeditor.api.response;


public class TokenInfo {
    private String mValue;
    private String mExpiredTime;

    public String getValue() {
        return mValue;
    }

    public String getExpiredTime() {
        return mExpiredTime;
    }
}
